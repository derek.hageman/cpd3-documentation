= CPD3 `_upload_` Configuration
:toc:

ifdef::cpd3-final[]

include::../shared/include_links.adoc[]

include::types/include_links.adoc[]

endif::cpd3-final[]


The `_upload_` variable in the `_CONFIGURATION_` archive contains the settings for the automatic data uploader.
Uploading involves calling one or more generator components, then using the standard upload backend to send their output to some location.
In some cases the generator may handle the uploading itself (e.g. the SQL output) and no actual files are uploaded.

The configuration is spit into profiles defining a single upload operation.
Each upload operation has a trigger of updated data that causes the upload to happen (e.g. new data are passed).
When the trigger is met, the upload is split into interval segments, with each interval segment being uploaded all at once.
Within the interval segment, it can be further split to prevent huge file generation but the whole interval segment will always be done at the same time.

== General Settings

:leveloffset: +1

ifdef::cpd3-final[]

include::metadata_upload_configuration.adoc[]

endif::cpd3-final[]

:leveloffset: -1