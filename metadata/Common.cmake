find_program(CPD3_DA NAMES da da.exe
             PATHS "${CPD3_BUILD}/cli")


function(convert_metadata variable)
    cmake_parse_arguments(generate "" "ARCHIVE" "" ${ARGN})
    if("${generate_ARCHIVE}" STREQUAL "")
        set(generate_ARCHIVE "configuration")
    endif("${generate_ARCHIVE}" STREQUAL "")

    set(output metadata_${variable}_${generate_ARCHIVE}.adoc)

    set(converter ${CMAKE_CURRENT_SOURCE_DIR}/convert.py)

    add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${output}
                       COMMAND ${CPD3_DA} --component=export --mode=xml _
                       ${variable} forever ${generate_ARCHIVE}_meta >
                       ${CMAKE_CURRENT_BINARY_DIR}/${output}.xml
                       COMMAND sync
                       COMMAND ${converter} ${CMAKE_CURRENT_BINARY_DIR}/${output}.xml
                       ${CMAKE_CURRENT_BINARY_DIR}/${output}
                       DEPENDS ${converter})

    add_custom_target(generate_metadata_${variable}_${generate_ARCHIVE}
                      DEPENDS "${CMAKE_CURRENT_BINARY_DIR}/${output}"
                      COMMENT "AsciiDoc metadata conversion for ${variable} ${generate_ARCHIVE}")

    add_dependencies(generate_metadata generate_metadata_${variable}_${generate_ARCHIVE})
endfunction(convert_metadata)