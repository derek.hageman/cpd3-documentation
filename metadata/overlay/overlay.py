#!/usr/bin/python3

def _describe(node, path):
    if 'type' not in node.attrib:
        result = list()
        result.append(path + " is absent")
        return result
    type = node.attrib['type'].lower()
    if type == 'hash':
        result = list()
        for c in node.findall('value'):
            cpath = c.attrib['key']
            result += _describe(c, path + "/" + cpath)
        return result
    elif type == 'array':
        result = list()
        index = 0
        for c in node.findall('value'):
            cpath = '#' + str(index)
            index = index + 1
            result += _describe(c, path + "/" + cpath)
        return result
    elif type == 'matrix':
        result = list()
        for c in node.findall('value'):
            cpath = "[" + c.attrib['index'].replace(',', ':') + "]"
            result += _describe(c, path + "/" + cpath)
        return result
    elif type == 'keyframe':
        result = list()
        for c in node.findall('value'):
            cpath = "/@" + c.attrib['key']
            result += _describe(c, path + "/" + cpath)
        return result
    elif type == 'real':
        result = list()
        if node.text is not None and len(node.text) > 0:
            result.append(path + " = " + str(float(node.text)))
        else:
            result.append(path + " is _undefined_")
        return result
    elif type == 'integer':
        result = list()
        if node.text is not None and len(node.text) > 0:
            result.append(path + " = " + long(int(node.text)))
        else:
            result.append(path + " is _undefined_")
        return result
    elif type == 'string':
        result = list()
        if node.text is not None and len(node.text) > 0:
            result.append(path + " = " + node.text)
        else:
            result.append(path + " is empty")
        return result
    elif type == 'boolean':
        result = list()
        result.append(path + " = " + node.text.upper())
        return result
    elif type == 'binary':
        result = list()
        if node.text is not None and len(node.text) > 0:
            result.append(path + " = " + node.text)
        else:
            result.append(path + " is empty")
        return result
    elif type == 'flags':
        result = list()
        flags = list()
        for f in node.find("flag"):
            flags += f.text
        result.append(path + " = |" + "|".join(flags))
        return result

class Overlay:
    def __init__(self, node, path, parent=None):
        self.node = node
        self.path = path
        self.data = node.find("value/[@key='Data']")
        if parent is not None:
            self.require = list(parent.require)
            self.exclude = list(parent.exclude)
        else:
            self.require = list()
            self.exclude = list()
        self.final = False
        try:
            self.final = (node.find("value/[@key='Final']").text.lower() == "true")
        except:
            pass

        v = node.find("value/[@key='DocumentationRequire']")
        if v is None:
            v = node.find("value/[@key='Require']")
        if v is not None:
            self.require += _describe(v, self.path)

        v = node.find("value/[@key='DocumentationExclude']")
        if v is None:
            v = node.find("value/[@key='Exclude']")
        if v is not None:
            self.exclude += _describe(v, self.path)

        self.section = None
        try:
            v = node.find("value/[@key='DocumentationSection']/[@type='string']")
            if v is not None:
                self.section = v.text or ""
        except:
            pass

    def write(self, output):
        if len(self.require) > 0:
            output.write("Required conditions:::\n+\n--\n")
            for c in self.require:
                output.write(" * " + c + "\n")
            output.write("--\n")

        if len(self.exclude) > 0:
            output.write("Excluding conditions:::\n+\n--\n")
            for c in self.exclude:
                output.write(" * " + c + "\n")
            output.write("--\n")