#!/usr/bin/python3

import sys
sys.dont_write_bytecode = True

import xml.etree.ElementTree
import collections

import handlers.base
import handlers.hash
import handlers.array
import handlers.matrix
import handlers.keyframe
import handlers.binary
import handlers.boolean
import handlers.real
import handlers.flags
import handlers.integer
import handlers.string
import handlers.arbitrary

import handlers.time
import handlers.smoother
import handlers.acquisition
import handlers.graphing
import handlers.editing
import handlers.network
import handlers.sequence
import handlers.algorithms



possible = [
    handlers.time.Interval,
    handlers.time.Absolute,
    handlers.smoother.BaselineSmoother,
    handlers.acquisition.AcquisitionIOInterface,
    handlers.acquisition.AcquisitionRemap,
    handlers.graphing.Color,
    handlers.graphing.Gradient,
    handlers.editing.WavelengthAdjust,
    handlers.editing.EditAction,
    handlers.editing.EditTrigger,
    handlers.network.SSL,
    handlers.algorithms.Model,
    handlers.algorithms.Transformer,

    handlers.hash.Hidden,
    handlers.hash.Profile,
    handlers.hash.UniqueKey,
    handlers.hash.SortedKey,
    handlers.hash.IntegerKey,
    handlers.hash.RegExpKey,
    handlers.hash.CalculationSetKey,

    handlers.string.Enumeration,
    handlers.array.Calibration,
    handlers.sequence.SequenceSelection,
    handlers.sequence.DynamicInput,
    handlers.arbitrary.DirectMetadata,
    handlers.arbitrary.ArbitraryStructure,

    # Generic complex types
    handlers.hash.Hash,
    handlers.array.Array,
    handlers.matrix.Matrix,
    handlers.keyframe.Keyframe,

    # Primitive types
    handlers.binary.Binary,
    handlers.boolean.Boolean,
    handlers.real.Real,
    handlers.flags.Flags,
    handlers.integer.Integer,
    handlers.string.String,
]

class _Output:
    class _Section:
        def __init__(self, heading=None):
            self.heading = heading
            self.contents = bytearray()

    def __init__(self):
        self._sections = [self._Section()]
        self._section_stack = [0]

    def write(self, s):
        self._sections[self._section_stack[-1]].contents += s.encode("utf-8")

    def push_section(self, name):
        sn = "=" * len(self._section_stack)
        sn += " "
        sn += name

        self._section_stack.append(len(self._sections))
        if len(name) > 0:
            self._sections.append(self._Section(sn))
        else:
            # This will make it just continue the previous section
            self._sections.append(self._Section())

    def pop_section(self):
        self._section_stack.pop()

    def finalize(self, target):
        for section in self._sections:
            if section.heading is not None:
                target.write(b'\n\n')
                target.write(section.heading.encode("utf-8"))
                target.write(b'\n\n')
            target.write(section.contents)


output = _Output()

def evalulate(path, node, overlay=None):
    for handler in possible:
        try:
            h = handler(node, path, overlay)
        except:
            continue

        section = h.section()
        if section is not None:
            output.push_section(section)

        for v in h.values():
            v.write(output)

        for childPath, childNode in h.children().items():
            effectivePath = path
            if not effectivePath.endswith('/'):
                effectivePath += '/'
            effectivePath += childPath
            evalulate(effectivePath, childNode, overlay)

        for o in h.overlays():
            if o.section is not None:
                output.push_section(o.section)
            evalulate(path, o.data, o)
            if o.section is not None:
                output.pop_section()

        if section is not None:
            output.pop_section()

        if not h.multiple():
            break


evalulate("/", xml.etree.ElementTree.parse(sys.argv[1]).getroot().find('value'))

with open(sys.argv[2], 'wb') as target:
    output.finalize(target)
