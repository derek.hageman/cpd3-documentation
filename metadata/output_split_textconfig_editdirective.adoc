= CPD3 Edit Directive Structure
:toc:

ifdef::cpd3-final[]

include::../shared/include_links.adoc[]

include::types/include_links.adoc[]

endif::cpd3-final[]


The variables in the `_EDITS_` archive define the mentor edits applied by the QA/QC (editing) system.

The specific variables are determined by the profile being edited.
For example, `_aerosol_` edits are applied in the profile of the corresponding name.
The structure of directives is always the same, regardless of the profile in use.

CAUTION: Directly changing directives should only be performed through the normal graphical interface.

== Structure

:leveloffset: +1

ifdef::cpd3-final[]

include::metadata___edits.adoc[]

endif::cpd3-final[]

:leveloffset: -1