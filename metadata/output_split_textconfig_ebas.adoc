= CPD3 `_ebas_` Configuration
:toc:

ifdef::cpd3-final[]

include::../shared/include_links.adoc[]

include::types/include_links.adoc[]

endif::cpd3-final[]


The `_ebas_` variable in the `_CONFIGURATION_` archive contains the definitions to generate EBAS formatted output files.

The configuration is spit into profiles defining sets of output files.
Each profile contains a list of individual output files that are all generated together (if the data in them exists) when the profile is generated.

For example, a standard profile contains one file definition for each possible size selection and level combination of all the instruments in the profile.

== General Settings

:leveloffset: +1

ifdef::cpd3-final[]

include::metadata_ebas_configuration.adoc[]

endif::cpd3-final[]

:leveloffset: -1