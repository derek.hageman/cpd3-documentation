[[data-type-acquisitioniointerface]]
= Acquisition I/O Interface

ifdef::textconfig-split[]

include::{cpd3-shared-directory}/include_links.adoc[]

include::{textconfig-type-directory}/include_links.adoc[]

endif::textconfig-split[]



An acquisition I/O interface is a specification of an input and output target.
This target is generally an external piece of hardware that the system is communicating with.
The most common example is a specification of a serial port to which an instrument is attached.

The basic structure of the interface consists of a type selection and various parameters for certain types.
The parameters for any type not currently selected are not valid and are ignored.

In addition to input and output streams of data, I/O interfaces have a concept of "echo".
That is, they can also report output written by other uses of the same interface.
This is normally used by instruments operated in read-only mode while listening to another source controlling the actual hardware.
For example, it is possible to have a CPD3 Love PID controller listening to CPD2 controlling the actual hardware and while using the echo stream it can interpret the command responses.

In some contexts, the whole specification of the interface is not required.
For example, the serial port parameters can be omitted for most acquisition contexts, because they are defined by the type of instrument actually connected.
Similarly, blacklisting or adding ports for auto-detection does not require the serial port parameters, since they only match the hardware port definition.

The type of the interface is defined by the <<{textconfig-type-link-enumeration},enumeration>> value `_Type_`:

`_SerialPort_`::
+
--
A locally connected physical serial port.
This is an input and output interface defined by the OS level name of the serial port.

`_Port_`:: The <<{shared-link-datum-string},text>> OS level name of the serial port.  For example "/dev/ttyUSB0" on Linux or "COM1" on Windows.
`_Baud_`:: The <<{shared-link-datum-integer},integer>> baud rate of the serial port.
`_Parity_`:: The <<{textconfig-type-link-enumeration},enumeration>> parity bit behaviour selection.  One of "None", "Even" or "Odd".
`_DataBits_`:: The <<{shared-link-datum-integer},integer>> number of data bits in a single frame.
`_StopBits_`:: The <<{shared-link-datum-integer},integer>> number of stop bits in a single frame.
`_HardwareFlowControl_`:: When this <<{shared-link-datum-boolean},boolean>> is enabled, the serial port will use the RTS and CTS signal lines for flow control.
`_SoftwareFlowControl_`:: The <<{textconfig-type-link-enumeration},enumeration>> software flow control setting.  One of "None" (XON/XOFF are normal characters; the default), "Bidirectional" (XON/XOFF used in both input and output), "Input" (input stream only), or "Output" (output stream only).
`_SoftwareFlowControlResume_`:: When this <<{shared-link-datum-boolean},boolean>> is enabled, flow control will resume on reception of any character (not just XON).  Meaningless unless input software flow control is in use.
`_NoInitialBreak_`:: When this <<{shared-link-datum-boolean},boolean>> is enabled, the initial break (line held in mark condition) on opening the serial port is omitted.
`_Eavesdropper_`:: The <<{shared-link-datum-string},text>> OS level name of the serial port to read eavesdropper echo data from.

--
`_RemoteServer_`::
+
--
A TCP connection to a remote server.
This type of interface passes input and output data over a TCP connection initiated to a provided server and port.
The most common use of this is to connect to a "serial to ethernet" type of converter box.
However, it can also be used to connect to any type of general TCP server to exchange data.

`_Server_`:: The <<{shared-link-datum-string},text>> IP address or host name to connect to.
`_ServerPort_`:: The <<{shared-link-datum-integer},integer>> port number to connect to.
`_SSL_`:: The <<{textconfig-type-link-ssl},ssl specification>> used with the connection.
--
`_CPD3Server_`::
+
--
A TCP connection to a CPD3 style I/O interface server.
This functionality allows an instance of the I/O server to be started on a remote machine then another one to connect to to it and control a local interface.
That is, this can be used as a replacement for a USB to IP converter type box, using a full computer to do the conversion.
For example, the machine with the hardware connected to it would start an I/O server on the connected ports, while having its configuration specify a valid remote server.
Then another one would have a `_CPD3Server_` style interface pointed at the first one.
This interface then is allowed to change serial port parameters and reconfigure the local hardware as required.

`_Server_`:: The <<{shared-link-datum-string},text>> IP address or host name to connect to.
`_ServerPort_`:: The <<{shared-link-datum-integer},integer>> port number to connect to.
`_SSL_`:: The <<{textconfig-type-link-ssl},ssl specification>> used with the connection.
--
`_Command_`::
+
--
Run an external command and communicate with it via its standard input and output channels.
This allows for an external command to be a sink and/or source of data to an I/O interface.
Note that the command may be restarted outside of its control (whenever the I/O interface re-initializes).
It should also not use buffered output or should take care to flush the buffers or the I/O interface will not receive data in a timely fashion.

`_Command_`:: The <<{shared-link-datum-string},text>> name of the executable to invoke.
`_Arguments_`/`_#0_`:: An array of <<{shared-link-datum-string},text>> arguments passed to the command.  For example, `_Arguments/#0_` is the first argument and `_Arguments/#1_` is the second.
--
`_TCPListen_`::
+
--
A server that listens for incoming TCP connections then exchanges data over any that are open.
This type of interface waits for an incoming TCP connection then will and and receive data to any connections that are currently open.

`_LocalPort_`:: The <<{shared-link-datum-integer},integer>> port number to listen on.
`_ListenAddress_`:: The <<{shared-link-datum-string},text>> IP address of the network to restrict listening on.  If set the the socket will only accept connections for the network interface associated with the given IP address.
`_SSL_`:: The <<{textconfig-type-link-ssl},ssl specification>> used with the connection.
`_SSL/Authorization_`/`_#0_`:: An array of SSL certificates permitted to connect.  When not present, only the basic SSL constraints are enforced.
--
`_UDP_`::
+
--
This interface sends and receives data via UDP datagram packets.
This type of interface accepts UDP packets for incoming data and takes their contents as a single input stream.
Likewise, it packages outgoing data into packets and sends them to a remote host.
Note that since the internal view of data is stream based, not packet based, the contents of where the UDP packets are split may not always coincide with single send and receives.

`_Server_`:: The <<{shared-link-datum-string},text>> IP address or host name to send data to.
`_ServerPort_`:: The <<{shared-link-datum-integer},integer>> destination port number to send data to.
`_LocalPort_`:: The <<{shared-link-datum-integer},integer>> local port to listen for incoming packets on.
`_ListenAddress_`:: The <<{shared-link-datum-string},text>> IP address of the network to restrict listening on.  If set the the socket will only accept connections for the network interface associated with the given IP address.
`_FragmentSize_`:: The <<{shared-link-datum-integer},integer>> maximum size of a single packet before it is split.
--
`_Multiplexer_`::
+
--
A multiplexer interface combines multiple sub-interfaces into a single composite one.
It allows for arbitrary mixing of input and output channels into a final view.
For example, it can be used to combine the input from two serial ports into a single logical stream.
Similarly, it can be used to construct an interface that sends data out one serial port but receives data from another physically distinct one.

Since the multiplexer only forwards data and not parameters, all sub-interfaces must provide all parameters needed to configure themselves.
For example, serial ports should provide a baud rate.

`_Targets/Input_`:: The <<{shared-link-datum-flags},set>> of group names that generate input for the composite interface.
`_Targets/Output_`:: The <<{shared-link-datum-flags},set>> of group names that outgoing data is assigned to.
`_Targets/Echo_`:: The <<{shared-link-datum-flags},set>> of group names that generate echo data for the composite interface.
`_Interfaces_`/`_#0_`:: An array of interface definitions that the multiplexer composites.
`_Interfaces_`/`_#0_`/`_Channels/Input_`:: The <<{shared-link-datum-flags},set>> of groups that input data on the sub-interface is assigned to.
`_Interfaces_`/`_#0_`/`_Channels/Output_`:: The <<{shared-link-datum-flags},set>> of groups that are matched against the composite ones when generating output.
`_Interfaces_`/`_#0_`/`_Channels/Echo_`:: The <<{shared-link-datum-flags},set>> of groups that echo data on the sub-interface is assigned to.
--
`_QtLocalSocket_`::
+
--
A bidirectional connection to a machine-local Qt-managed socket.

`_Name_`:: The <<{shared-link-datum-string},text>> name of the socket to connect to.
--
`_QtLocalListen_`::
+
--
A bidirectional connection from a machine-local Qt-managed socket.
This creates a socket that listens for other machine local communications then sends and receives data over all open connections.

`_Name_`:: The <<{shared-link-datum-string},text>> name of the socket to connect to.
--
`_LocalSocket_`::
+
--
*Only supported on Unix*.
A bidirectional connection to a local Unix domain socket.

`_File_`:: The <<{shared-link-datum-string},file name>> of the Unix domain socket.
--
`_LocalListen_`::
+
--
*Only supported on Unix*.
A bidirectional connection from a local Unix domain socket.
This creates a socket that listens for other machine local communications then sends and receives data over all open connections.

`_File_`:: The <<{shared-link-datum-string},file name>> of the Unix domain socket.
--
`_Pipe_`::
+
--
*Only supported on Unix*.
This type of interface communicates with a set of Unix named pipes (FIFOs) that represent the three channels of communications.
That is, it send data to the output pipe, and reads input and echo from their respective pipes.

`_Input_`:: The <<{shared-link-datum-string},file name>> of the Unix named pipe to read input data from.
`_Output_`:: The <<{shared-link-datum-string},file name>> of the Unix named pipe to write output data to.
`_Echo_`:: The <<{shared-link-datum-string},file name>> of the Unix named pipe to read echo data from.
--


.Serial port with automatic parameters
----
/Type,"SerialPort"
/Port,"/dev/ttyUSB1"
----

.Serial port with manual parameters
----
/Type,"SerialPort"
/Port,"/dev/ttyUSB1"
/Baud,115200
/Parity,"None"
/StopBits,2
/DataBits,8
----

.Remote server connection
----
/Type,"RemoteServer"
/Server,"192.168.0.111"
/ServerPort,12341
----

.Local command communication
----
/Type,"Command"
/Command,"cat"
/Arguments/#0,"-"
----

.Split multiplexer output
----
/Type,"Multiplexer"
/Targets/Input,|in
/Targets/Output,|out
/Interfaces/#0/Channels/Input,|in
/Interfaces/#0/Type,"SerialPort"
/Interfaces/#0/Port,"/dev/ttyUSB0"
/Interfaces/#0/Baud,9600
/Interfaces/#1/Channels/Output,|out
/Interfaces/#1/Type,"SerialPort"
/Interfaces/#1/Port,"/dev/ttyUSB1"
/Interfaces/#1/Baud,9600
----