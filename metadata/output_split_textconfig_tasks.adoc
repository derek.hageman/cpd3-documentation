= CPD3 `_tasks_` Configuration
:toc:

ifdef::cpd3-final[]

include::../shared/include_links.adoc[]

include::types/include_links.adoc[]

endif::cpd3-final[]


The `_tasks_` variable in the `_CONFIGURATION_` archive contains the settings for automatic tasks to run.

The tasks defined by the configuration are run by the "tasks" component at various intervals.
Each task has a priority that determines how frequently it is invoked.

== General Settings

:leveloffset: +1

ifdef::cpd3-final[]

include::metadata_tasks_configuration.adoc[]

endif::cpd3-final[]

:leveloffset: -1