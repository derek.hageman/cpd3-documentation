#!/usr/bin/python3

import handlers.base as B
import handlers.value as V
import handlers.hash as H
import collections


class Color(H.Hash):
    def __init__(self, node, path, overlay=None):
        H.Hash.__init__(self, node, path, overlay)
        if self.editorType != "color":
            raise ValueError()

    class _StructureValue(V.Value):
        def __init__(self, node, path, overlay=None):
            V.Value.__init__(self, node, path, overlay)

        def write(self, output):
            V.Value.write(self, output)
            output.write("Type::: <<{textconfig-type-link-color},Color>>\n")
            output.write("Structure:::\n+\n--\n")
            output.write("A color as displayed on the screen or in an image.\n")
            output.write("Colors are determined by their channel values, each ranging from zero to one, inclusive.\n")
            output.write("For example, black has all components zero, except alpha (transparency) which is one.\n")
            output.write("The color model in use is determined by the `_Model_` <<{textconfig-type-link-enumeration},enumeration>> key.\n")
            output.write("--\n")

    def values(self):
        result = B.Base.values(self)
        result.append(self._StructureValue(self.node, self.path, self.overlay))
        return result

    def children(self):
        return self.collapsed_children()

    def overlays(self):
        return self.collapsed_overlays()


class Gradient(H.Hash):
    def __init__(self, node, path, overlay=None):
        H.Hash.__init__(self, node, path, overlay)
        if self.editorType != "gradient":
            raise ValueError()

    class _StructureValue(V.Value):
        def __init__(self, node, path, overlay=None):
            V.Value.__init__(self, node, path, overlay)

        def write(self, output):
            V.Value.write(self, output)
            output.write("Type::: <<{textconfig-type-link-gradient},Gradient>>\n")
            output.write("Structure:::\n+\n--\n")
            output.write("A gradient is an interpolator that converts values into a spectrum of colors.\n")
            output.write("Several standard gradients are available, as well as a definition based on key colors.\n")
            output.write("The type of gradient used is selected by the `_Type_` <<{textconfig-type-link-enumeration},enumeration>> key.\n")
            output.write("--\n")

    def values(self):
        result = B.Base.values(self)
        result.append(self._StructureValue(self.node, self.path, self.overlay))
        return result

    def children(self):
        return self.collapsed_children()

    def overlays(self):
        return self.collapsed_overlays()

