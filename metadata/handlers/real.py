#!/usr/bin/python3

import handlers.base as B
import handlers.value as V


class Real(B.Base):
    def __init__(self, node, path, overlay=None):
        B.Base.__init__(self, node, path, overlay)
        if self.dataType != "metareal":
            raise ValueError()

    class _Value(V.Value):
        def __init__(self, node, path, overlay=None):
            V.Value.__init__(self, node, path, overlay)

        def write(self, output):
            V.Value.write(self, output)
            output.write("Type::: <<{shared-link-datum-real},Real number>>\n")

            minimum = self.editor_real("Minimum")
            maximum = self.editor_real("Maximum")
            if minimum is not None or maximum is not None:
                output.write("Valid range::: ")
                if minimum is not None:
                    output.write(str(minimum))
                    if self.editor_bool("MinimumExclusive"):
                        output.write(" < ")
                    else:
                        output.write(" ≤ ")
                output.write("value")
                if maximum is not None:
                    if self.editor_bool("MaximumExclusive"):
                        output.write(" < ")
                    else:
                        output.write(" ≤ ")
                    output.write(str(maximum))
                output.write("\n")

            units = V.to_string(self.node, "meta/[@key='Units']/[@type='string']")
            if units is not None:
                output.write("Units:::\n+\n--\n" + units + "\n--\n")

            if not self.editor_bool("AllowUndefined"):
                output.write("Undefined values::: Not permitted\n")
            else:
                meaning = V.to_string(self.node, "meta/[@key='UndefinedDescription']/[@type='string']")
                if meaning is not None:
                    output.write("Undefined values:::\n+\n--\n" + meaning + "\n--\n")

    def values(self):
        result = B.Base.values(self)
        result.append(self._Value(self.node, self.path, self.overlay))
        return result
