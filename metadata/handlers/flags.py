#!/usr/bin/python3

import handlers.base as B
import handlers.value as V


class Flags(B.Base):
    def __init__(self, node, path, overlay=None):
        B.Base.__init__(self, node, path, overlay)
        if self.dataType != "metaflags":
            raise ValueError()

    class _Value(V.Value):
        def __init__(self, node, path, overlay=None):
            V.Value.__init__(self, node, path, overlay)

        def write(self, output):
            V.Value.write(self, output)
            output.write("Type::: <<{shared-link-datum-flags},Flags>>, a set of unique text strings\n")
            valid_flags = dict()
            for f in self.node.findall("value/[@type='hash']"):
                name = f.attrib['key']
                description = ""
                try:
                    description = f.find("value/[@key='Description']").text
                except:
                    pass
                valid_flags[name] = description
            if len(valid_flags) > 0:
                output.write("Acceptable Flags:::\n+\n--\n[horizontal]\n")
                keys = list(valid_flags.keys())
                keys.sort()
                for k in keys:
                    output.write(k + ":::: " + valid_flags[k] + "\n")
                output.write("--\n")

    def values(self):
        result = B.Base.values(self)
        result.append(self._Value(self.node, self.path, self.overlay))
        return result