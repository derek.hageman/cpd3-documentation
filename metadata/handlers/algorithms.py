#!/usr/bin/python3

import handlers.base as B
import handlers.value as V
import handlers.hash as H
import collections


class Model(H.Hash):
    def __init__(self, node, path, overlay=None):
        H.Hash.__init__(self, node, path, overlay)
        if self.editorType != "model":
            raise ValueError()

    class _StructureValue(V.Value):
        def __init__(self, node, path, overlay=None):
            V.Value.__init__(self, node, path, overlay)

        def write(self, output):
            V.Value.write(self, output)
            output.write("Type::: <<{textconfig-type-link-model},Interpolation Model>>\n")
            output.write("Structure:::\n+\n--\n")
            output.write("A definition of a model used for interpolation.\n")
            output.write("Interpolation models are used to generate interpolations or fits of some sequence of input.\n")
            output.write("For example, a linear regression and a cubic spline interpolator are both available.\n")
            output.write("The the type of model is set with the `_Type_` <<{textconfig-type-link-enumeration},enumeration>> key.\n")
            output.write("--\n")

    def values(self):
        result = B.Base.values(self)
        result.append(self._StructureValue(self.node, self.path, self.overlay))
        return result

    def children(self):
        return self.collapsed_children()

    def overlays(self):
        return self.collapsed_overlays()


class Transformer(H.Hash):
    def __init__(self, node, path, overlay=None):
        H.Hash.__init__(self, node, path, overlay)
        if self.editorType != "transformer":
            raise ValueError()

    class _StructureValue(V.Value):
        def __init__(self, node, path, overlay=None):
            V.Value.__init__(self, node, path, overlay)

        def write(self, output):
            V.Value.write(self, output)
            output.write("Type::: <<{textconfig-type-link-transformer},Data Transformer>>\n")
            output.write("Structure:::\n+\n--\n")
            output.write("A data transformer is a general construct that applies an transformation function to some sequence of points.\n")
            output.write("Examples include data filtering (removing points) and converting data into various probability distribution functions.\n")
            output.write("The conversion type is selected with the `_Type_` <<{textconfig-type-link-enumeration},enumeration>> key.\n")
            output.write("--\n")

    def values(self):
        result = B.Base.values(self)
        result.append(self._StructureValue(self.node, self.path, self.overlay))
        return result

    def children(self):
        return self.collapsed_children()

    def overlays(self):
        return self.collapsed_overlays()

