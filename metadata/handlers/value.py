#!/usr/bin/python3

def to_real(node, path, default=None):
    try:
        return float(node.find(path).text)
    except:
        return default

def to_integer(node, path, default=None):
    try:
        return int(node.find(path).text)
    except:
        return default

def to_bool(node, path, default=False):
    try:
        return node.find(path).text.lower() == "true"
    except:
        return default

def to_string(node, path, default=None):
    try:
        return node.find(path).text
    except:
        return default

class Value:
    def __init__(self, node, path, overlay=None):
        self.node = node
        self.path = path
        self.overlay = overlay
        self.editor = node.find("meta/[@key='Editor']/[@type='hash']")

    def write(self, output):
        output.write(self.path + "::\n")
        if self.overlay is not None:
            self.overlay.write(output)
        try:
            description = self.node.find("meta/[@key='Description']").text
            output.write("Description:::\n+\n--\n" + description + "\n--\n")
        except:
            pass
        try:
            default = self.node.find("meta/[@key='Default']").text
            output.write("Default:::\n+\n--\n" + default + "\n--\n")
        except:
            pass

    def editor_real(self, key, default=None):
        return to_real(self.editor, "value/[@key='%s']" % key, default)

    def editor_integer(self, key, default=None):
        return to_integer(self.editor, "value/[@key='%s']" % key, default)

    def editor_bool(self, key, default=None):
        return to_bool(self.editor, "value/[@key='%s']" % key, default)