#!/usr/bin/python3

import handlers.hash as H
import handlers.real as R
import handlers.value as V
import collections

class Interval(H.Hash):
    def __init__(self, node, path, overlay=None):
        H.Hash.__init__(self, node, path, overlay)
        if self.editorType != "timeinterval" and self.editorType != "dynamictimeinterval":
            raise ValueError()

    class _StructureValue(V.Value):
        def __init__(self, node, path, overlay=None):
            V.Value.__init__(self, node, path, overlay)

        def write(self, output):
            V.Value.write(self, output)
            output.write("Type::: <<{textconfig-type-link-timeinterval},Time Interval>>\n")

            disable_count = self.editor_bool("DisableCount")
            disable_align = self.editor_bool("DisableAlign")
            allow_zero = self.editor_bool("AllowZero")
            allow_negative = self.editor_bool("AllowNegative")
            default_aligned = self.editor_bool("DefaultAligned")

            output.write("Structure:::\n+\n--\n")
            if disable_count and disable_align:
                output.write("A time interval consists of a time unit specification.\n")
            elif disable_align:
                output.write("A time interval consists of a time unit and a number of units to apply.\n")
            elif disable_count:
                output.write("A time interval consists of a time unit specification and an alignment setting.\n")
            else:
                output.write("A time interval consists of a time unit, a number of units to apply, and an alignment setting.\n")
            if not disable_count:
                if allow_zero:
                    output.write("The number of units applied can be zero; this is normally only used for forcing alignment.\n")
                if allow_negative:
                    output.write("The number of units applied can be negative.\n")
            if not disable_align and default_aligned:
                output.write("Alignment is enabled by default.\n")
            if self.editor_bool("Composite"):
                output.write("Additional keys are accepted as part of a higher level composite structure.\n")
            output.write("--\n")

            meaning = V.to_string(self.node, "meta/[@key='UndefinedDescription']/[@type='string']")
            if meaning is not None:
                output.write("Undefined values:::\n+\n--\n" + meaning + "\n--\n")

    def values(self):
        result = H.Hash.values(self)
        result.append(self._StructureValue(self.node, self.path, self.overlay))
        return result

    def children(self):
        result = self.collapsed_children()

        unit = self.node.find("value/[@key='Units']")
        if unit is not None:
            result['Units'] = unit

        count = self.node.find("value/[@key='Count']")
        if count is not None and not self.editor_bool("DisableCount"):
            result['Count'] = count

        align = self.node.find("value/[@key='Align']")
        if align is not None and not self.editor_bool("DisableAlign"):
            result['Align'] = align

        if self.editor_bool("Composite"):
            add = H.Hash.children(self)
            for k, v in add.items():
                if k == 'Units' or k == 'Count' or k == 'Align':
                    continue
                result[k] = v

        return result


class Absolute(R.Real):
    def __init__(self, node, path, overlay=None):
        R.Real.__init__(self, node, path, overlay)
        if self.editorType != "absolutetime":
            raise ValueError()

    class _StructureValue(V.Value):
        def __init__(self, node, path, overlay=None):
            V.Value.__init__(self, node, path, overlay)

        def write(self, output):
            V.Value.write(self, output)

            output.write("Structure:::\n+\n--\n")
            output.write("This is an absolute point in time specified as the number of seconds from the Unix epoch (1970-01-01T00:00:00Z).\n")
            output.write("--\n")

    def values(self):
        result = R.Real.values(self)
        result.append(self._StructureValue(self.node, self.path, self.overlay))
        return result