#!/usr/bin/python3

import handlers.base as B
import handlers.value as V
import handlers.hash as H


class SequenceSelection(B.Base):
    def __init__(self, node, path, overlay=None):
        B.Base.__init__(self, node, path, overlay)
        if self.editorType != "sequenceselection" and self.editorType != "dynamicsequencename":
            raise ValueError()

    class _StructureValue(V.Value):
        def __init__(self, node, path, overlay=None):
            V.Value.__init__(self, node, path, overlay)

        def write(self, output):
            V.Value.write(self, output)
            output.write("Type::: <<{textconfig-type-link-sequenceselection},Sequence Selection>>\n")
            output.write("Structure:::\n+\n--\n")
            output.write("A sequence selection is a means of selecting a sequence of values in the data.\n")
            output.write("The specification consists of selections for the various components of the sequence name.\n")
            output.write("--\n")

            units = V.to_string(self.node, "meta/[@key='Units']/[@type='string']")
            if units is not None:
                output.write("Units:::\n+\n--\n" + units + "\n--\n")

    def values(self):
        result = B.Base.values(self)
        result.append(self._StructureValue(self.node, self.path, self.overlay))
        return result

    def children(self):
        return dict()

    def overlays(self):
        return list()


class DynamicInput(B.Base):
    def __init__(self, node, path, overlay=None):
        B.Base.__init__(self, node, path, overlay)
        if self.editorType != "dynamicinput":
            raise ValueError()

    class _StructureValue(V.Value):
        def __init__(self, node, path, overlay=None):
            V.Value.__init__(self, node, path, overlay)

        def write(self, output):
            V.Value.write(self, output)
            output.write("Type::: <<{textconfig-type-link-dynamicinput},Dynamic Real Value Input>>\n")
            output.write("Structure:::\n+\n--\n")
            output.write("This is a specification of a single real valued input used in calculation.\n")
            output.write("The specification generally takes the form of either a value from the data or a constant.\n")
            output.write("--\n")

            minimum = self.editor_real("Minimum")
            maximum = self.editor_real("Maximum")
            if minimum is not None or maximum is not None:
                output.write("Expected::: ")
                if minimum is not None:
                    output.write(str(minimum))
                    if self.editor_bool("MinimumExclusive"):
                        output.write(" < ")
                    else:
                        output.write(" ≤ ")
                output.write("value")
                if maximum is not None:
                    if self.editor_bool("MaximumExclusive"):
                        output.write(" < ")
                    else:
                        output.write(" ≤ ")
                    output.write(str(maximum))
                output.write("\n")

            units = V.to_string(self.node, "meta/[@key='Units']/[@type='string']")
            if units is not None:
                output.write("Units:::\n+\n--\n" + units + "\n--\n")

    def values(self):
        result = B.Base.values(self)
        result.append(self._StructureValue(self.node, self.path, self.overlay))
        return result

    def children(self):
        return dict()

    def overlays(self):
        return list()
