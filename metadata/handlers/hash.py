#!/usr/bin/python3

import handlers.base as B
import handlers.value as V
import overlay.overlay as O
import collections


class Hash(B.Base):
    def __init__(self, node, path, overlay=None):
        B.Base.__init__(self, node, path, overlay)
        if self.dataType != "metahash":
            raise ValueError()

    def collapsed_children(self):
        result = dict()
        for child in self.node.findall("value"):
            key = child.attrib['key']
            if key == "":
                continue
            if not V.to_bool(child, "meta/[@key='Editor']/[@type='hash']/value/[@key='DocumentationExpand']"):
                continue
            result[key] = child
        result = collections.OrderedDict(sorted(result.items(), key=lambda t: t[0]))
        return result

    def collapsed_overlays(self):
        result = list()
        for o in self.node.findall("meta/[@key='Overlay']/value"):
            if not V.to_bool(o, "value/[@key='DocumentationExpand']"):
                continue
            result.append(O.Overlay(o, self.path, self.overlay))
        return result

    def children(self):
        result = dict()
        for child in self.node.findall("value"):
            key = child.attrib['key']
            if key == "":
                continue
            result[key] = child
        result = collections.OrderedDict(sorted(result.items(), key=lambda t: t[0]))
        return result


class Hidden(Hash):
    def __init__(self, node, path, overlay=None):
        Hash.__init__(self, node, path, overlay)
        if not self.editor_bool("HideDocumentation"):
            raise ValueError()

    def values(self):
        result = B.Base.values(self)
        result.append(V.Value(self.node, self.path, self.overlay))
        return result

    def children(self):
        return dict()

    def overlays(self):
        return list()


class Profile(Hash):
    def __init__(self, node, path, overlay=None):
        Hash.__init__(self, node, path, overlay)
        if self.editorType != "profile":
            raise ValueError()

    class _KeysValue(V.Value):
        def __init__(self, node, path, overlay=None):
            V.Value.__init__(self, node, path, overlay)

        def write(self, output):
            V.Value.write(self, output)
            output.write("Keys:::\n+\n--\n")
            output.write("The keys immediately below this are profile names that are selected by the application.\n")
            output.write("For example, `_aerosol_` is the commonly used default profile.\n")
            output.write("However, any non-empty name is acceptable and can be used to create customized configurations that can be selected at run time.\n")
            output.write("In general, profile names should be all lower case to preserve case insensitivity.\n")
            output.write("In the paths listed `_<PROFILE>_` is used as a placeholder to real profile name (e.g. replace `_<PROFILE>_` with `_aerosol_` for the default profile).\n")
            output.write("--\n")

    def values(self):
        result = B.Base.values(self)
        result.append(self._KeysValue(self.node, self.path, self.overlay))
        return result

    def children(self):
        result = collections.OrderedDict()
        all = self.node.find("value/[@key='']")
        if all is not None:
            result['`_<PROFILE>_`'] = all
        return result


class UniqueKey(Hash):
    def __init__(self, node, path, overlay=None):
        Hash.__init__(self, node, path, overlay)
        if self.editorType != "uniquekey":
            raise ValueError()

    class _KeysValue(V.Value):
        def __init__(self, node, path, overlay=None):
            V.Value.__init__(self, node, path, overlay)

        def write(self, output):
            V.Value.write(self, output)
            output.write("Keys:::\n+\n--\n")
            output.write("The keys immediately below this are unique names selected by the user.\n")
            output.write("These names can be any non-empty text and are generally used to designate different components to be configured.\n")
            output.write("In the paths listed `_<IDENTIFIER>_` is used as a placeholder to real name selected during configuration.\n")
            output.write("--\n")

    def values(self):
        result = B.Base.values(self)
        result.append(self._KeysValue(self.node, self.path, self.overlay))
        return result

    def children(self):
        result = collections.OrderedDict()
        all = self.node.find("value/[@key='']")
        if all is not None:
            result['`_<IDENTIFIER>_`'] = all
        return result


class SortedKey(Hash):
    def __init__(self, node, path, overlay=None):
        Hash.__init__(self, node, path, overlay)
        if self.editorType != "sortedkey":
            raise ValueError()

    class _KeysValue(V.Value):
        def __init__(self, node, path, overlay=None):
            V.Value.__init__(self, node, path, overlay)

        def write(self, output):
            V.Value.write(self, output)
            output.write("Keys:::\n+\n--\n")
            output.write("The keys immediately below this are unique numbers selected by the user.\n")
            output.write("These keys are evaluated in sorted numeric order, with smaller numbers coming first.\n")
            output.write("This is distinct from array notation in that it allows for skipped numbers and for partial overriding of a specific key only.\n")
            output.write("In the paths listed `_<NUMBER>_` is used as a placeholder to real name selected during configuration.\n")
            output.write("--\n")

    def values(self):
        result = B.Base.values(self)
        result.append(self._KeysValue(self.node, self.path, self.overlay))
        return result

    def children(self):
        result = collections.OrderedDict()
        all = self.node.find("value/[@key='']")
        if all is not None:
            result['`_<NUMBER>_`'] = all
        return result

class IntegerKey(Hash):
    def __init__(self, node, path, overlay=None):
        Hash.__init__(self, node, path, overlay)
        if self.editorType != "integerkey":
            raise ValueError()

    class _KeysValue(V.Value):
        def __init__(self, node, path, overlay=None):
            V.Value.__init__(self, node, path, overlay)

        def write(self, output):
            V.Value.write(self, output)
            output.write("Keys:::\n+\n--\n")
            output.write("The keys immediately below this are unique integers selected by the user.\n")
            output.write("They are used to look up specific values for a given integer input.\n")
            output.write("This is distinct from array notation in that it allows for skipped numbers and for partial overriding of a specific key only.\n")
            output.write("In the paths listed `_<INTEGER>_` is used as a placeholder to real name selected during configuration.\n")
            output.write("--\n")

            minimum = self.editor_integer("Minimum")
            maximum = self.editor_integer("Maximum")
            if minimum is not None or maximum is not None:
                output.write("Valid range::: ")
                if minimum is not None:
                    output.write(str(minimum))
                    output.write(" ≤ ")
                output.write("value")
                if maximum is not None:
                    output.write(" ≤ ")
                    output.write(str(maximum))
                output.write("\n")

    def values(self):
        result = B.Base.values(self)
        result.append(self._KeysValue(self.node, self.path, self.overlay))
        return result

    def children(self):
        result = collections.OrderedDict()
        all = self.node.find("value/[@key='']")
        if all is not None:
            result['`_<INTEGER>_`'] = all
        return result


class RegExpKey(Hash):
    def __init__(self, node, path, overlay=None):
        Hash.__init__(self, node, path, overlay)
        if self.editorType != "regularexpression":
            raise ValueError()

    class _KeysValue(V.Value):
        def __init__(self, node, path, overlay=None):
            V.Value.__init__(self, node, path, overlay)

        def write(self, output):
            V.Value.write(self, output)
            output.write("Keys:::\n+\n--\n")
            output.write("The keys immediately below this are regular expressions matched against the input.\n")
            output.write("These regular expressions are non-empty text that constructs a pattern matcher.\n")
            output.write("In the simplest form, this is just the full text, but more complex expressions are supported.\n")
            output.write("In the paths listed `_<REGEXP>_` is used as a placeholder to real value used during configuration.\n")
            output.write("--\n")

    def values(self):
        result = B.Base.values(self)
        result.append(self._KeysValue(self.node, self.path, self.overlay))
        return result

    def children(self):
        result = collections.OrderedDict()
        all = self.node.find("value/[@key='']")
        if all is not None:
            result['`_<REGEXP>_`'] = all
        return result


class CalculationSetKey(Hash):
    def __init__(self, node, path, overlay=None):
        Hash.__init__(self, node, path, overlay)
        if self.editorType == "editingcomponent":
            if self.editor_bool("CalculationSet"):
                return
        raise ValueError()

    class _KeysValue(V.Value):
        def __init__(self, node, path, overlay=None):
            V.Value.__init__(self, node, path, overlay)

        def write(self, output):
            V.Value.write(self, output)
            output.write("Keys:::\n+\n--\n")
            output.write("The keys immediately below this are unique names selected by the user.\n")
            output.write("These names can be any non-empty text and are generally used to designate different independent calculation sets to be configured.\n")
            output.write("In general each calculation set is merged to the same time base within the set.\n")
            output.write("For example, a common set is all the variables of the same size and averaging for an instrument.\n")
            output.write("In the paths listed `_<CALCULATION>_` is used as a placeholder to real name selected during configuration.\n")
            output.write("--\n")

    def values(self):
        result = B.Base.values(self)
        result.append(self._KeysValue(self.node, self.path, self.overlay))
        return result

    def children(self):
        result = collections.OrderedDict()
        all = self.node.find("value/[@key='']")
        if all is not None:
            result['`_<CALCULATION>_`'] = all
        return result


class ComponentOptions(Hash):
    def __init__(self, node, path, overlay=None):
        Hash.__init__(self, node, path, overlay)
        if self.editorType != "componentoptions":
            raise ValueError()

    class _KeysValue(V.Value):
        def __init__(self, node, path, overlay=None):
            V.Value.__init__(self, node, path, overlay)

        def write(self, output):
            V.Value.write(self, output)
            output.write("Keys:::\n+\n--\n")
            output.write("The keys immediately below this are internal option names.\n")
            output.write("These names can be any non-empty text and correspond to the internal identifiers of the component options.\n")
            output.write("In the paths listed `_<OPTION>_` is used as a placeholder to real name of the option.\n")
            output.write("--\n")

    def values(self):
        result = B.Base.values(self)
        result.append(self._KeysValue(self.node, self.path, self.overlay))
        return result

    def children(self):
        result = collections.OrderedDict()
        all = self.node.find("value/[@key='']")
        if all is not None:
            result['`_<OPTION>_`'] = all
        return result
