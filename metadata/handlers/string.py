#!/usr/bin/python3

import handlers.base as B
import handlers.value as V


class String(B.Base):
    def __init__(self, node, path, overlay=None):
        B.Base.__init__(self, node, path, overlay)
        if self.dataType != "metastring":
            raise ValueError()

    class _Value(V.Value):
        def __init__(self, node, path, overlay=None):
            V.Value.__init__(self, node, path, overlay)

        def write(self, output):
            V.Value.write(self, output)
            output.write("Type::: <<{shared-link-datum-string},Text>>\n")

            meaning = V.to_string(self.node, "meta/[@key='UndefinedDescription']/[@type='string']")
            if meaning is not None:
                output.write("Undefined values:::\n+\n--\n" + meaning + "\n--\n")

    def values(self):
        result = B.Base.values(self)
        result.append(self._Value(self.node, self.path, self.overlay))
        return result

class Enumeration(String):

    class _EnumerationValue:
        def __init__(self, node):
            self.sort_priority = V.to_integer(node, "value/[@key='SortPriority']")
            self.name = node.attrib['key']
            self.description = V.to_string(node, "value/[@key='Description']")
            self.alias = [ ]
            for a in node.findall("value/[@key='Alias']/[@type='flags']/flag"):
                self.alias.append(a.text)

        def list_value(self, output):
            output.write(" * `_%s_`" % self.name)
            if len(self.alias) == 1:
                output.write(" or `_%s_`" % self.alias[0])
            elif len(self.alias) > 1:
                for i in range(0, len(self.alias) - 1):
                    output.write(", `_%s_`" % self.alias[i])
                output.write(", or `_%s_`" % self.alias[-1])
            if self.description is not None:
                output.write(" - %s" % self.description)
            output.write("\n")

        def _compare_to(self, other):
            if self.sort_priority is not None:
                if other.sort_priority is None:
                    return -1
                if self.sort_priority < other.sort_priority:
                    return -1
                elif self.sort_priority > other.sort_priority:
                    return 1
            elif other.sort_priority is not None:
                return 1
            v = self.name.lower()
            o = other.name.lower()
            if v < o:
                return -1
            elif v > o:
                return 1
            return 0

        def __lt__(self, other):
            return self._compare_to(other) < 0
        def __gt__(self, other):
            return self._compare_to(other) > 0
        def __eq__(self, other):
            return self._compare_to(other) == 0
        def __le__(self, other):
            return self._compare_to(other) <= 0
        def __ge__(self, other):
            return self._compare_to(other) >= 0
        def __ne__(self, other):
            return self._compare_to(other) != 0

    def __init__(self, node, path, overlay=None):
        String.__init__(self, node, path, overlay)
        if self.editorType != "enumeration":
            raise ValueError()
        self.enumeration = [ ]
        for value in self.node.findall("meta/[@key='EnumerationValues']/value/[@type='hash']"):
            self.enumeration.append(self._EnumerationValue(value))
        if len(self.enumeration) == 0:
            raise ValueError()
        self.enumeration.sort()


    class _PossibleValues(V.Value):
        def __init__(self, node, path, overlay=None, enumeration=list()):
            V.Value.__init__(self, node, path, overlay)
            self.enumeration = enumeration

        def write(self, output):
            V.Value.write(self, output)
            output.write("Type::: <<{textconfig-type-link-enumeration},Enumeration>>\n")
            output.write("Values:::\n+\n--\n")
            output.write("This is an enumeration value.\n")
            output.write("It accepts a single string (not case sensitive) from a list of possible choices.\n")
            if len(self.enumeration) > 0:
                output.write("Possible values:\n\n")
                for v in self.enumeration:
                    v.list_value(output)
            output.write("--\n")

    def values(self):
        result = B.Base.values(self)
        result.append(self._PossibleValues(self.node, self.path, self.overlay, self.enumeration))
        return result