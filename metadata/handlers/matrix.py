#!/usr/bin/python3

import handlers.base as B
import handlers.value as V
import collections


class Matrix(B.Base):
    def __init__(self, node, path, overlay=None):
        B.Base.__init__(self, node, path, overlay)
        if self.dataType != "metaarray":
            raise ValueError()
        self.size = []
        for s in self.node.findall("meta/[@key='Size']/[@type='array']/value/[@type='integer']"):
            try:
                t = int(s.text)
            except:
                continue
            if t > 0:
                self.size.append(t)

    class _KeysValue(V.Value):
        def __init__(self, node, path, overlay=None, size=[]):
            V.Value.__init__(self, node, path, overlay)
            self.size = size

        def write(self, output):
            V.Value.write(self, output)
            output.write("Keys:::\n+\n--\n")
            output.write("The keys immediately below this are matrix indices.\n")
            output.write("These are indexes into an ordered matrix, with the first element of each dimension being zero.\n")
            key = '`_[0'
            if len(self.size) > 1:
                for s in self.size[1:]:
                    key += ":0"
            key += ']_`'
            output.write("For example, " + key + " is the first element for all dimensions in the matrix.\n")
            output.write("The paths represented here use " + key + " as a placeholder for all indices.\n")
            if len(self.size) > 0:
                dimensions = str(self.size[0])
                for s in self.size[1:]:
                    dimensions += ":" + str(s)
                output.write("The expected dimensions of the matrix are " + dimensions + ".\n")
            output.write("--\n")

    def values(self):
        result = B.Base.values(self)
        result.append(self._KeysValue(self.node, self.path, self.overlay))
        return result

    def children(self):
        result = collections.OrderedDict()
        all = self.node.find("meta/[@key='Children']")
        if all is not None:
            key = '`_[0'
            if len(self.size) > 1:
                for s in self.size[1:]:
                    key += ":0"
            key += ']_`'
            result[key] = all
        return result