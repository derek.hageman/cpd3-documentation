#!/usr/bin/python3

import handlers.base as B
import handlers.value as V


class Boolean(B.Base):
    def __init__(self, node, path, overlay=None):
        B.Base.__init__(self, node, path, overlay)
        if self.dataType != "metaboolean":
            raise ValueError()

    class _Value(V.Value):
        def __init__(self, node, path, overlay=None):
            V.Value.__init__(self, node, path, overlay)

        def write(self, output):
            V.Value.write(self, output)
            output.write("Type::: <<{shared-link-datum-boolean},Boolean>>, values are `_TRUE_` or `_FALSE_`")
            if self.node.find("meta/[@key='Default']") is None:
                output.write(" and defaults to `_FALSE_`")
            output.write("\n")

    def values(self):
        result = B.Base.values(self)
        result.append(self._Value(self.node, self.path, self.overlay))
        return result
