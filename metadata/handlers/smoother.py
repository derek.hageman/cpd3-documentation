#!/usr/bin/python3

import handlers.base as B
import handlers.value as V
import handlers.hash as H


class BaselineSmoother(H.Hash):
    def __init__(self, node, path, overlay=None):
        H.Hash.__init__(self, node, path, overlay)
        if self.editorType != "baselinesmoother":
            raise ValueError()

    class _StructureValue(V.Value):
        def __init__(self, node, path, overlay=None):
            V.Value.__init__(self, node, path, overlay)

        def write(self, output):
            V.Value.write(self, output)
            output.write("Type::: <<{textconfig-type-link-baselinesmoother},Baseline Smoother>>\n")
            output.write("Structure:::\n+\n--\n")
            output.write("A baseline smoother is a construct that is used to generate a smoothed output based on an input value sequence.\n")
            output.write("The possible valid configuration parameters are determined by the `_Type_` <<{textconfig-type-link-enumeration},enumeration>> key.\n")
            if self.editor_bool("EnableStability"):
                output.write("The smoother is used for stability detection.\n")
                output.write("This means that the type of smoother selected should provide an indicator of when the smoothed value is stable.\n")
            if self.editor_bool("EnableSpike"):
                output.write("The smoother is used for spike detection.\n")
                output.write("This means that the type of smoother selected should provide an indicator of when the incoming value is considered a spike.\n")
            output.write("--\n")

            meaning = V.to_string(self.node, "meta/[@key='UndefinedDescription']/[@type='string']")
            if meaning is not None:
                output.write("Undefined values:::\n+\n--\n" + meaning + "\n--\n")

    def values(self):
        result = B.Base.values(self)
        result.append(self._StructureValue(self.node, self.path, self.overlay))
        return result

    def children(self):
        return self.collapsed_children()

    def overlays(self):
        return self.collapsed_overlays()