#!/usr/bin/python3

import overlay.overlay as O
import handlers.value as V
import collections

class Base:
    def __init__(self, node, path, o=None):
        self.node = node
        self.path = path
        self.overlay = o
        self.dataType = node.attrib['type'].lower()
        self.editor = node.find("meta/[@key='Editor']/[@type='hash']")
        self.editorType = None
        try:
            self.editorType = self.editor.find("value/[@key='Type']").text.lower()
        except:
            pass

    class _TemplateValue(V.Value):
        def __init__(self, node, path, overlay=None, template="/"):
            V.Value.__init__(self, node, path, overlay)
            self.template = template

        def write(self, output):
            V.Value.write(self, output)
            output.write("Template Target::: `_" + self.template + "_`\n")

    def values(self):
        try:
            template = self.node.find("meta/[@key='TemplatePath']/[@type='string']").text
            if len(template) > 0:
                result = list()
                result.append(self._TemplateValue(self.node, self.path, self.overlay, template))
                return result
        except:
            pass
        return list()

    def children(self):
        return dict()

    def overlays(self):
        result = list()
        for o in self.node.findall("meta/[@key='Overlay']/value"):
            result.append(O.Overlay(o, self.path, self.overlay))
        return result

    def multiple(self):
        return False

    def section(self):
        try:
            v = self.editor.find("value/[@key='Section']/[@type='string']")
            if v is not None:
                return v.text or ""
        except:
            pass
        return None


    def editor_real(self, key, default=None):
        return V.to_real(self.editor, "value/[@key='%s']" % key, default)

    def editor_integer(self, key, default=None):
        return V.to_integer(self.editor, "value/[@key='%s']" % key, default)

    def editor_bool(self, key, default=None):
        return V.to_bool(self.editor, "value/[@key='%s']" % key, default)