#!/usr/bin/python3

import handlers.base as B
import handlers.value as V
import handlers.hash as H
import collections


class AcquisitionIOInterface(H.Hash):
    def __init__(self, node, path, overlay=None):
        H.Hash.__init__(self, node, path, overlay)
        if self.editorType != "acquisitioniointerface":
            raise ValueError()

    class _StructureValue(V.Value):
        def __init__(self, node, path, overlay=None):
            V.Value.__init__(self, node, path, overlay)

        def write(self, output):
            V.Value.write(self, output)
            output.write("Type::: <<{textconfig-type-link-acquisitioniointerface},I/O Interface>>\n")
            output.write("Structure:::\n+\n--\n")
            output.write("An I/O interface is a general purpose endpoint used to communicate with an external system.\n")
            output.write("The simplest case is a local serial port connected to an instrument.\n")
            output.write("The possible valid configuration parameters are determined by the `_Type_` <<{textconfig-type-link-enumeration},enumeration>> key.\n")
            output.write("--\n")

    def values(self):
        result = B.Base.values(self)
        result.append(self._StructureValue(self.node, self.path, self.overlay))
        return result

    def children(self):
        return self.collapsed_children()

    def overlays(self):
        return self.collapsed_overlays()


class AcquisitionRemap(H.Hash):
    def __init__(self, node, path, overlay=None):
        H.Hash.__init__(self, node, path, overlay)
        if self.editorType != "acquisitionremap":
            raise ValueError()

    class _KeysValue(V.Value):
        def __init__(self, node, path, overlay=None):
            V.Value.__init__(self, node, path, overlay)

        def write(self, output):
            V.Value.write(self, output)
            output.write("Keys:::\n+\n--\n")
            output.write("The keys immediately below this are internal variable names.\n")
            output.write("These variables are the internal identifiers used by the acquisition component to control its acquisition.\n")
            output.write("In most cases the final output values correspond to just their name without the suffix.\n")
            output.write("For example `_BaG_A11_` might have the internal identifier `_BaG_`.\n")
            output.write("In the paths listed `_<VARIABLE>_` is used as a placeholder to real value used during configuration.\n")
            output.write("--\n")

    def values(self):
        result = B.Base.values(self)
        result.append(self._KeysValue(self.node, self.path, self.overlay))
        return result

    def children(self):
        result = collections.OrderedDict()
        all = self.node.find("value/[@key='']")
        if all is not None:
            result['`_<VARIABLE>_`'] = all
        return result