#!/usr/bin/python3

import handlers.base as B
import handlers.value as V
import collections


class Array(B.Base):
    def __init__(self, node, path, overlay=None):
        B.Base.__init__(self, node, path, overlay)
        if self.dataType != "metaarray":
            raise ValueError()

    class _KeysValue(V.Value):
        def __init__(self, node, path, overlay=None):
            V.Value.__init__(self, node, path, overlay)

        def write(self, output):
            V.Value.write(self, output)

            output.write("Keys:::\n+\n--\n")
            output.write("The keys immediately below this are array indices.\n")
            output.write("These are indexes into an ordered list, with the first element being zero.\n")
            output.write("For example, `_#0_` is the first element of the array.\n")
            output.write("The paths represented here use `_#0_` as a placeholder for all indices.\n")
            try:
                count = int(self.node.find("meta/[@key='Count']/[@type='integer']").text)
                if count > 1:
                    output.write("The array is expected to contain " + str(count) + " values.\n")
            except:
                pass
            output.write("--\n")

    def values(self):
        result = B.Base.values(self)
        result.append(self._KeysValue(self.node, self.path, self.overlay))
        return result

    def children(self):
        result = collections.OrderedDict()
        all = self.node.find("meta/[@key='Children']")
        if all is not None:
            result['`_#0_`'] = all
        return result


class Calibration(Array):
    def __init__(self, node, path, overlay=None):
        Array.__init__(self, node, path, overlay)
        if self.editorType != "calibration":
            raise ValueError()

    class _StructureValue(V.Value):
        def __init__(self, node, path, overlay=None):
            V.Value.__init__(self, node, path, overlay)

        def write(self, output):
            V.Value.write(self, output)
            output.write("Type::: <<{textconfig-type-link-calibration},Calibration>>\n")
            output.write("Structure:::\n+\n--\n")
            output.write("A calibration is an array with the coefficients being the array values in ascending power order.\n")
            output.write("All keys are array indices with <<{shared-link-datum-real},real number>> values.\n")
            output.write("For example, `_#0_` is the constant offset while `_#1_` is the slope.\n")
            output.write("--\n")

    def values(self):
        result = B.Base.values(self)
        result.append(self._StructureValue(self.node, self.path, self.overlay))
        return result

    def children(self):
        return dict()