#!/usr/bin/python3

import handlers.base as B
import handlers.value as V
import collections


class Keyframe(B.Base):
    def __init__(self, node, path, overlay=None):
        B.Base.__init__(self, node, path, overlay)
        if self.dataType != "metaarray":
            raise ValueError()

    class _KeysValue(V.Value):
        def __init__(self, node, path, overlay=None):
            V.Value.__init__(self, node, path, overlay)

        def write(self, output):
            V.Value.write(self, output)
            output.write("Keys:::\n+\n--\n")
            output.write("The keys immediately below this are keyframe elements.\n")
            output.write("These are real numbers that map in an ordered way to other values.\n")

            minimum = None
            minimum_exclusive = False
            try:
                minimum = float(self.editor.find("value/[@key='Minimum']").text)
                minimum_exclusive = self.editor.find("value/[@key='MinimumExclusive']").text.lower() == "true"
            except:
                pass
            maximum = None
            maximum_exclusive = False
            try:
                maximum = float(self.editor.find("value/[@key='Maximum']").text)
                maximum_exclusive = self.editor.find("value/[@key='MaximumExclusive']").text.lower() == "true"
            except:
                pass

            key = '`_@0_`'
            if minimum is not None:
                if not minimum_exclusive:
                    key = '`_@' + str(minimum) + '_`'
                elif maximum is not None:
                    key = '`_@' + str((minimum + maximum) * 0.5) + '_`'
                else:
                    key = '`_@' + str(minimum + 1.0) + '_`'
            elif maximum is not None:
                if not maximum_exclusive:
                    key = '`_@' + str(maximum) + '_`'
                else:
                    key = '`_@' + str(maximum - 1.0) + '_`'

            output.write("For example, " + key + " is a mapped position in the keyframe.\n")
            output.write("The paths represented here use " + key + " as a placeholder for all mappings.\n")
            if minimum is not None:
                output.write("Values must be greater than ")
                if not minimum_exclusive:
                    output.write("or equal to ")
                output.write(str(minimum))
                if maximum is not None:
                    output.write(" and less than ")
                    if not maximum_exclusive:
                        output.write("or equal to ")
                    output.write(str(maximum))
                output.write(".\n")
            elif maximum is not None:
                output.write("Values must be less than ")
                if not maximum_exclusive:
                    output.write("or equal to ")
                    output.write(str(maximum))
                output.write(".\n")
            output.write("--\n")

    def values(self):
        result = B.Base.values(self)
        result.append(self._KeysValue(self.node, self.path, self.overlay))
        return result

    def children(self):
        result = collections.OrderedDict()
        all = self.node.find("meta/[@key='Children']")
        if all is not None:
            minimum = None
            minimum_exclusive = False
            try:
                minimum = float(self.editor.find("value/[@key='Minimum']").text)
                minimum_exclusive = self.editor.find("value/[@key='MinimumExclusive']").text.lower() == "true"
            except:
                pass
            maximum = None
            maximum_exclusive = False
            try:
                maximum = float(self.editor.find("value/[@key='Maximum']").text)
                maximum_exclusive = self.editor.find("value/[@key='MaximumExclusive']").text.lower() == "true"
            except:
                pass

            key = '`_@0_`'
            if minimum is not None:
                if not minimum_exclusive:
                    key = '`_@' + str(minimum) + '_`'
                elif maximum is not None:
                    key = '`_@' + str((minimum + maximum) * 0.5) + '_`'
                else:
                    key = '`_@' + str(minimum + 1.0) + '_`'
            elif maximum is not None:
                if not maximum_exclusive:
                    key = '`_@' + str(maximum) + '_`'
                else:
                    key = '`_@' + str(maximum - 1.0) + '_`'
            result[key] = all
        return result