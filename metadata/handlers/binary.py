#!/usr/bin/python3

import handlers.base as B
import handlers.value as V


class Binary(B.Base):
    def __init__(self, node, path, overlay=None):
        B.Base.__init__(self, node, path, overlay)
        if self.dataType != "metabinary":
            raise ValueError()

    class _Value(V.Value):
        def __init__(self, node, path, overlay=None):
            V.Value.__init__(self, node, path, overlay)

        def write(self, output):
            V.Value.write(self, output)
            output.write("Type::: <<{shared-link-datum-binary},Binary>>, data are raw bytes\n")

    def values(self):
        result = B.Base.values(self)
        result.append(self._Value(self.node, self.path, self.overlay))
        return result