#!/usr/bin/python3

import handlers.base as B
import handlers.value as V
import handlers.hash as H
import collections


class WavelengthAdjust(H.Hash):
    def __init__(self, node, path, overlay=None):
        H.Hash.__init__(self, node, path, overlay)
        if self.editorType != "wavelengthadjust":
            raise ValueError()

    class _StructureValue(V.Value):
        def __init__(self, node, path, overlay=None):
            V.Value.__init__(self, node, path, overlay)

        def write(self, output):
            V.Value.write(self, output)
            output.write("Type::: <<{textconfig-type-link-wavelengthadjust},Wavelength Adjuster>>\n")
            output.write("Structure:::\n+\n--\n")
            output.write("A wavelength adjuster is a construct that provides optical coefficient wavelength dependend functions.\n")
            output.write("The most common case is to simply adjust the coefficients to different wavelengths using interpolation.\n")
            output.write("Additional uses include Ångström exponent generation and smoothing control.\n")
            output.write("The adjuster consists of various inputs and their parameters.\n")
            output.write("--\n")
            smoothing = V.to_string(self.node, "meta/[@key='DefaultSmoothing']/[@type='string']")
            if smoothing is not None:
                output.write("Default smoothing:::\n+\n--\n" + smoothing + "\n--\n")

    def values(self):
        result = B.Base.values(self)
        result.append(self._StructureValue(self.node, self.path, self.overlay))
        return result

    def children(self):
        return self.collapsed_children()

    def overlays(self):
        return self.collapsed_overlays()


class EditTrigger(H.Hash):
    def __init__(self, node, path, overlay=None):
        H.Hash.__init__(self, node, path, overlay)
        if self.editorType != "edittrigger":
            raise ValueError()

    class _StructureValue(V.Value):
        def __init__(self, node, path, overlay=None):
            V.Value.__init__(self, node, path, overlay)

        def write(self, output):
            V.Value.write(self, output)
            output.write("Type::: <<{textconfig-type-link-edittrigger},Trigger>>\n")
            output.write("Structure:::\n+\n--\n")
            output.write("An edit trigger is a means of detecting a condition in the data stream.\n")
            output.write("For example, it can compare a value to a threshold and only take action when the value exceeds the threshold.\n")
            output.write("--\n")

    def values(self):
        result = B.Base.values(self)
        result.append(self._StructureValue(self.node, self.path, self.overlay))
        return result

    def children(self):
        return self.collapsed_children()

    def overlays(self):
        return self.collapsed_overlays()


class EditAction(H.Hash):
    def __init__(self, node, path, overlay=None):
        H.Hash.__init__(self, node, path, overlay)
        if self.editorType != "editaction":
            raise ValueError()

    class _StructureValue(V.Value):
        def __init__(self, node, path, overlay=None):
            V.Value.__init__(self, node, path, overlay)

        def write(self, output):
            V.Value.write(self, output)
            output.write("Type::: <<{textconfig-type-link-editaction},Editing Action>>\n")
            output.write("Structure:::\n+\n--\n")
            output.write("An edit action is an operation performed on the data stream.\n")
            output.write("The most common example is imply invalidation: setting the affected data to explicitly missing values.\n")
            output.write("--\n")

    def values(self):
        result = B.Base.values(self)
        result.append(self._StructureValue(self.node, self.path, self.overlay))
        return result

    def children(self):
        return self.collapsed_children()

    def overlays(self):
        return self.collapsed_overlays()

