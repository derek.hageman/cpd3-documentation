#!/usr/bin/python3

import handlers.base as B
import handlers.value as V
import collections


class ArbitraryStructure(B.Base):
    def __init__(self, node, path, overlay=None):
        B.Base.__init__(self, node, path, overlay)
        if not V.to_bool(self.node, "meta/[@key='ArbitraryStructure']", False):
            raise ValueError()

    class _KeysValue(V.Value):
        def __init__(self, node, path, overlay=None):
            V.Value.__init__(self, node, path, overlay)

        def write(self, output):
            V.Value.write(self, output)
            output.write("Keys:::\n+\n--\n")
            output.write("The keys below this one have an arbitrary structure.\n")
            output.write("That is, the exact contents are not directly interpreted by the system so they are defined by conventions or subsequent usage.\n")
            output.write("--\n")

    def values(self):
        result = B.Base.values(self)
        result.append(self._KeysValue(self.node, self.path, self.overlay))
        return result

    def children(self):
        result = dict()
        for child in self.node.findall("value"):
            key = child.attrib['key']
            if key == "":
                continue
            result[key] = child
        result = collections.OrderedDict(sorted(result.items(), key=lambda t: t[0]))
        return result


class DirectMetadata(ArbitraryStructure):
    def __init__(self, node, path, overlay=None):
        ArbitraryStructure.__init__(self, node, path, overlay)
        if self.editorType != "metadata":
            raise ValueError()
        try:
            self.dataType = self.editor.find("value/[@key='DataType']").text.lower()
        except:
            pass

    class _KeysValue(V.Value):
        def __init__(self, node, path, overlay=None):
            V.Value.__init__(self, node, path, overlay)

        def write(self, output):
            V.Value.write(self, output)
            output.write("Structure:::\n+\n--\n")
            output.write("This value represents metadata injected directly into some output.\n")
            output.write("That is, the exact contents defined by this value are used as metadata for another value.\n")
            output.write("The exact keys are determined by what metadata is required.\n")
            output.write("--\n")

    def values(self):
        result = B.Base.values(self)
        result.append(self._KeysValue(self.node, self.path, self.overlay))
        return result

    def children(self):
        prefix = ""
        if self.dataType == 'real':
            prefix = "*d"
        elif self.dataType == 'integer':
            prefix = "*i"
        elif self.dataType == 'boolean':
            prefix = "*b"
        elif self.dataType == 'string':
            prefix = "*s"
        elif self.dataType == 'binary':
            prefix = "*n"
        elif self.dataType == 'array':
            prefix = "*a"
        elif self.dataType == 'matrix':
            prefix = "*t"
        elif self.dataType == 'keyframe':
            prefix = "*e"
        elif self.dataType == 'hash':
            prefix = "*h"
        elif self.dataType == 'flags':
            prefix = "*f"

        result = dict()
        for child in self.node.findall("value"):
            key = child.attrib['key']
            if key == "":
                continue
            result[prefix + key] = child
        result = collections.OrderedDict(sorted(result.items(), key=lambda t: t[0]))
        return result