#!/usr/bin/python3

import handlers.hash as H
import handlers.value as V

class SSL(H.Hash):
    def __init__(self, node, path, overlay=None):
        H.Hash.__init__(self, node, path, overlay)
        if self.editorType != "ssl":
            raise ValueError()

    class _StructureValue(V.Value):
        def __init__(self, node, path, overlay=None):
            V.Value.__init__(self, node, path, overlay)

        def write(self, output):
            V.Value.write(self, output)
            output.write("Type::: <<{textconfig-type-link-ssl},SSL Specification>>\n")

            enable_validity = self.editor_bool("EnableValidity")

            output.write("Structure:::\n+\n--\n")
            output.write("A SSL specification consists of a hash map defining the parameters for encrypted SSL/TLS connections.\n")
            if enable_validity:
                output.write("The specification also permits enforcing peer certificate validity.\n")
            output.write("--\n")

    def values(self):
        result = H.Hash.values(self)
        result.append(self._StructureValue(self.node, self.path, self.overlay))
        return result

    def children(self):
        result = H.Hash.children(self)

        if "Certificate" in result:
            del result["Certificate"]
        if "Key" in result:
            del result["Key"]
        if "Authority" in result:
            del result["Authority"]
        if "NoLocal" in result:
            del result["NoLocal"]
        if "MinimumVersion" in result:
            del result["MinimumVersion"]
        if "RequireValid" in result:
            del result["RequireValid"]

        return result