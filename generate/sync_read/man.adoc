= DA.SYNC.READ(1)
:man source:  CPD3
:man version: {revnumber}
:man manual:  CPD3 Manual


== NAME

da.sync.read - Read a synchronization stream


== SYNOPSIS

*da.sync.read* [_OPTIONS_...] [station] [file]


== DESCRIPTION

This program reads a synchronization stream created with da.sync.write(1) and applies the changes to the local archive.
It is used with manual transfer of that stream to accomplish synchronization of two archives.

== OPTIONS

ifdef::cpd3-final[]

include::options_man.adoc[]

endif::cpd3-final[]


== ARGUMENTS

*station*::
+
--
This argument is used to specify the station that synchronization is accepted for.
This is the GAW station code of the site, such as _BND_ or _MLO_.
The station code is not case sensitive.
--
*file*::
+
--
This argument is used to specify the the file to write data to.
If it is absent or set to _-_ (a single dash) then the stream is written to standard output.
--


ifdef::cpd3-final[]

include::{cpd3-shared-directory}/man_tail.adoc[]

endif::cpd3-final[]
