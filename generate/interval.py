#!/usr/bin/python3

import enum

class Units(enum.Enum):
    millisecond = 1
    second = 2
    minute = 3
    hour = 4
    day = 5
    week = 6
    month = 7
    quarter = 8
    year = 9

    def printable(self, count=1):
        if self == Units.millisecond:
            if count != 1:
                return "milliseconds"
            return "millisecond"
        if self == Units.second:
            if count != 1:
                return "seconds"
            return "second"
        if self == Units.minute:
            if count != 1:
                return "minutes"
            return "minute"
        if self == Units.hour:
            if count != 1:
                return "hours"
            return "hour"
        if self == Units.day:
            if count != 1:
                return "days"
            return "day"
        if self == Units.week:
            if count != 1:
                return "weeks"
            return "week"
        if self == Units.month:
            if count != 1:
                return "months"
            return "month"
        if self == Units.quarter:
            if count != 1:
                return "quarters"
            return "quarter"
        if self == Units.year:
            if count != 1:
                return "years"
            return "year"

    def short(self):
        if self == Units.millisecond:
            return "msec"
        if self == Units.second:
            return "s"
        if self == Units.minute:
            return "m"
        if self == Units.hour:
            return "h"
        if self == Units.day:
            return "d"
        if self == Units.week:
            return "w"
        if self == Units.month:
            return "mo"
        if self == Units.quarter:
            return "q"
        if self == Units.year:
            return "y"
    
class Interval:
    def __init__(self, input):
        self.aligned = None
        self.unit = None
        self.count = None
        if input is None:
            return

        if input.get("aligned") == "true":
            self.aligned = True
        unit = input.get("unit", default="").lower()
        if unit == "millisecond":
            self.unit = Units.millisecond
        elif unit == "second":
            self.unit = Units.second
        elif unit == "minute":
            self.unit = Units.minute
        elif unit == "hour":
            self.unit = Units.hour
        elif unit == "day":
            self.unit = Units.day
        elif unit == "week":
            self.unit = Units.week
        elif unit == "month":
            self.unit = Units.month
        elif unit == "quarter":
            self.unit = Units.quarter
        elif unit == "year":
            self.unit = Units.year
        if input.text is not None:
            value = input.text.strip()
            if value != "":
                self.count = int(value)

    def format(self):
        result = ""
        if self.count is not None:
            result += str(self.count)
        if self.unit is not None:
            result += self.unit.short()
        if self.aligned:
            result += "a"
        return result