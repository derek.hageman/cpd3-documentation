#!/usr/bin/python3

import sys
import xml.etree.ElementTree
import xmlloader
import identifier

input = xmlloader.load(sys.argv[1], sys.argv[3:])
output = open(sys.argv[2], "w", encoding="utf-8")

htype = identifier.to_type(input)

def _to_integer(input):
    if input is None or input.text is None:
        return None
    return int(input.text)

if htype == "filter" or htype == "read" or htype == "external" or htype == "input" or htype == "action" or htype == "archive":
    component = input.find("identifier/component").text
    name = "DA." + component.replace("_", ".").upper()
    output.write("= %s(1)\n" % name)

    command = "da." + component.replace("_", ".").lower()
elif htype == "display":
    component = input.find("identifier/output").text
    if input.find("options/file/[@id='file']") is not None:
        name = "DA.PLOT." + component.replace("_", ".").upper()
        command = "da.plot." + component.replace("_", ".").lower()
    else:
        name = "DA.SHOW." + component.replace("_", ".").upper()
        command = "da.show." + component.replace("_", ".").lower()

    output.write("= %s(1)\n" % name)
else:
    name = input.find("command/program")
    if name is not None:
        command = name.text.strip()
        name = name.upper()
    else:
        raise Exception("No command available")
    output.write("= %s(1)\n" % name)
output.write(":man source:  CPD3\n")
output.write(":man version: {revnumber}\n")
output.write(":man manual:  CPD3 Manual\n")
output.write("\n\n")


def _has_options():
    return len(input.findall("options/*")) > 0

def _has_examples():
    return len(input.findall("examples/*")) > 0

def _has_barewords():
    if htype == "filter" or htype == "archive" or htype == "read" or htype == "display":
        return True
    elif htype == "external" or htype == "input" or htype == "action":
        if input.find("identifier/requiredStations") is not None:
            return True
        if input.find("identifier/allowedStations") is not None:
            return True
        if input.find("identifier/time") is not None:
            return True
        return False
    return False


def _write_name():
    output.write("== NAME\n\n")
    output.write(command)
    description = input.find("identifier/display")
    if description is not None:
        output.write(" - %s\n" % description.text)
        return
    output.write("\n")

def _write_synopsis():
    output.write("== SYNOPSIS\n\n")
    output.write("*%s*" % command)
    if _has_options():
        output.write(" [_OPTION_...]")

    if htype == "filter":
        output.write(" [[station] variables times [archive]|[file]]")
    elif htype == "archive":
        output.write(" [station] variables times [archive]")
    elif htype == "display":
        output.write(" [[station] [variables] times [archive]|[file]]")
    elif htype == "read":
        output.write(" [file]")
    elif htype == "external" or htype == "input" or htype == "action":
        require = _to_integer(input.find("identifier/requiredStations"))
        if require is None:
            require = 0
        allow = input.find("identifier/allowedStations")
        if allow is None:
            allow = 0
        else:
            allow = _to_integer(allow)
        if require == 0:
            if allow == 1:
                output.write(" [station]")
            elif allow != 0:
                output.write(" [station...]")
        elif require > 0:
            if allow == 1:
                if require == 1:
                    # Inference
                    output.write(" [station]")
                else:
                    output.write(" station")
            elif allow != 0:
                output.write(" station...")

        times = input.find("identifier/time")
        if times is not None:
            if times.text.lower() == "optional":
                output.write(" [times]")
            else:
                output.write(" times")

    output.write("\n\n")

def _write_description():
    output.write("== DESCRIPTION\n\n")
    description = input.find("identifier/description")
    if description is not None:
        output.write("%s\n" % description.text)
        return
    description = input.find("command/description")
    if description is not None:
        output.write("%s\n" % description.text)
        return
    output.write("\n")

def _write_barewords():
    output.write("== ARGUMENTS\n\n")

    def _write_shared_type(name, type=None):
        if type is None:
            type = name
        output.write("*%s*::\n" % name)
        output.write("+\n")
        output.write("--\n")
        output.write("include::{cpd3-shared-directory}/man_%s.adoc[]\n" % type)
        output.write("--\n")

    if htype == "filter" or htype == "display":
        output.write("If no bare word input specification is supplied then data are read from standard input.\n")
        output.write("\n\n")
        _write_shared_type("station")
        _write_shared_type("variables")
        _write_shared_type("times")
        _write_shared_type("archive")
        _write_shared_type("file")
    elif htype == "archive":
        _write_shared_type("station")
        _write_shared_type("variables")
        _write_shared_type("times")
        _write_shared_type("archive")
    elif htype == "read":
        _write_shared_type("file")
        output.write("\n\n")
        output.write("If no file is supplied then the input is read from standard input.")
    elif htype == "external" or htype == "input" or htype == "action":
        require = _to_integer(input.find("identifier/requiredStations"))
        if require is None:
            require = 0
        allow = input.find("identifier/allowedStations")
        if require > 0 or allow is not None:
            _write_shared_type("station")

        if input.find("identifier/time") is not None:
            _write_shared_type("times")


def _write_options():
    output.write("== OPTIONS\n\n")
    output.write("include::options_man.adoc[]\n\n")

def _write_examples():
    output.write("== EXAMPLES\n\n")
    output.write("include::examples_man.adoc[]\n\n")

_write_name()
output.write("\n\n")

_write_synopsis()
output.write("\n\n")

_write_description()
output.write("\n\n")

if _has_options():
    _write_options()
    output.write("\n\n")

if _has_barewords():
    _write_barewords()
    output.write("\n\n")

if _has_examples():
    _write_examples()
    output.write("\n\n")

output.write("include::{cpd3-shared-directory}/man_tail.adoc[]\n\n")