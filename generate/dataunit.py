#!/usr/bin/python3
    
class DataUnit:
    def __init__(self, input):
        self.station = None
        self.archive = None
        self.variable = None
        self.flavors = None
        if input is None:
            return

        c = input.find("station")
        if c is not None:
            self.station = c.text.lower()

        c = input.find("archive")
        if c is not None:
            self.archive = c.text.lower()

        c = input.find("variable")
        if c is not None:
            self.variable = c.text

        if input.find("flavors"):
            self.flavors = [ ]
            for f in input.findall("flavors/flavor"):
                self.flavors.append(f.text)

    def components(self):
        result = []
        if self.station is not None:
            result.append(self.station)
        if self.archive is not None:
            result.append(self.archive)
        if self.variable is not None:
            result.append(self.variable)
        if self.flavors is not None:
            for f in self.flavors:
                result.append(f)
        return result