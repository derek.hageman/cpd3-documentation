#!/usr/bin/python3

import interval
import dataunit
import re
import calendar
import time

def format_double(input):
    if int(input) == input:
        return str(int(input))
    return str(input)

def format_time(point):
    if point is None:
        return "undef"
    if int(point / 86400) * 86400 == point:
        return "%04d-%02d-%02d" % time.gmtime(point)[0:3]
    return "%04d-%02d-%02dT%02d:%02d:%02dZ" % time.gmtime(point)[0:6]

def _get_origin(example, options):
    id = example.get("id")
    return options.find("*/[@id='" + id + "']")


def _to_float(input):
    if input is None or input.text is None:
        return None
    return float(input.text)


def _to_integer(input):
    if input is None or input.text is None:
        return None
    return int(input.text)


def _to_time(input):
    if input is None or input.text is None:
        return None
    fields = re.findall('\d+', input.text)
    return calendar.timegm(tuple(map(int, fields[0:6])))


def _unroll_list(list, cnv):
    result = []
    for e in list:
        result.append(cnv(e))
    return result


class Examples:
    def __init__(self, examples, options, output):
        self.examples = examples
        self.options = options
        self._output = output

    def write(self, data):
        self._output.write(data)

    class Example:
        def __init__(self, example):
            self.example = example
            self.options = example.findall('options/*')
            self.title = example.find('title').text
            self.description = example.find('description')
            if self.description is not None:
                self.description = self.description.text

            self.station = example.find('input/station').text.lower()
            self.archive = example.find('input/archive').text.lower()
            self.time_start = _to_time(example.find('input/start'))
            self.time_end = _to_time(example.find('input/end'))
            self.scattering = example.find('input/scattering') is not None
            self.absorption = example.find('input/absorption') is not None
            self.counts = example.find('input/counts') is not None
            self.file = example.find('input/file') is not None
            self.auxiliary = _unroll_list(example.findall('input/auxiliary/value'), lambda r: r.text)

        def write(self, data):
            self.parent.write(data)

        def begin(self):
            self.write("%s::\n" % self.title)
            if self.description is not None:
                self.write("+\n")
                self.write("--\n")
                self.write("%s\n" % self.description.strip())
                self.write("\n")
                self.write("--\n")
            self.write("+\n")
            self.write("--\n")

        def end(self):
            self.write("--\n")

        def run_options(self):
            for option in self.options:
                option_type = option.tag.lower()

                origin = _get_origin(option, self.parent.options)
                name = origin.find("name").text
                display = origin.find("display").text
                description = origin.find("description").text

                if option_type == "boolean":
                    self.option_boolean(option, origin, name, display, description,
                                        option.find("value").text == "true")
                    continue
                if option_type == "file":
                    self.option_file(option, origin, name, display, description,
                                     option.find("value").text)
                    continue
                if option_type == "directory":
                    self.option_directory(option, origin, name, display, description,
                                          option.find("value").text)
                    continue
                if option_type == "script":
                    self.option_script(option, origin, name, display, description,
                                       option.find("value").text)
                    continue
                if option_type == "string":
                    self.option_string(option, origin, name, display, description,
                                       option.find("value").text)
                    continue
                if option_type == "double":
                    self.option_double(option, origin, name, display, description,
                                       _to_float(option.find("value")))
                    continue
                if option_type == "doubleset":
                    self.option_double_set(option, origin, name, display, description,
                                           _unroll_list(option.findall("values/value"), _to_float))
                    continue
                if option_type == "doublelist":
                    self.option_double_list(option, origin, name, display, description,
                                            _unroll_list(option.findall("values/value"), _to_float))
                    continue
                if option_type == "integer":
                    self.option_integer(option, origin, name, display, description,
                                        _to_integer(option.find("value")))
                    continue
                if option_type == "instrument":
                    self.option_instrument(option, origin, name, display, description,
                                           _unroll_list(option.findall("values/value"), lambda r: r.text))
                    continue
                if option_type == "stringset":
                    self.option_string_set(option, origin, name, display, description,
                                           _unroll_list(option.findall("values/value"), lambda r: r.text))
                    continue
                if option_type == "time":
                    self.option_time(option, origin, name, display, description,
                                     _to_time(option.find("value")))
                    continue
                if option_type == "calibration":
                    self.option_calibration(option, origin, name, display, description,
                                            _unroll_list(option.findall("coefficients/coefficient"), _to_float))
                    continue
                if option_type == "variable":
                    self.option_variable(option, origin, name, display, description,
                                         dataunit.DataUnit(option.find("selected")))
                    continue
                if option_type == "input":
                    v = _to_float(option.find("value"))
                    if v is not None:
                        self.option_input(option, origin, name, display, description, v)
                        continue
                    self.option_input(option, origin, name, display, description,
                                      dataunit.DataUnit(option.find("selected")))
                    continue
                if option_type == "operate":
                    self.option_operate(option, origin, name, display, description,
                                        dataunit.DataUnit(option.find("selected")))
                    continue
                if option_type == "enum":
                    check = option.find("value").get("id")
                    selected = None
                    for v in origin.findall("values/value"):
                        id = v.get("id")
                        if id != check:
                            continue
                        selected = dict()
                        selected["id"] = id
                        selected["name"] = v.find("name").text
                        selected["description"] = v.find("description").text

                    self.option_enum(option, origin, name, display, description,
                                     selected)
                    continue
                if option_type == "interval":
                    self.option_interval(option, origin, name, display, description,
                                         interval.Interval(option.find("value")))
                    continue
                if option_type == "intervalblock":
                    self.option_interval_block(option, origin, name, display, description,
                                               interval.Interval(option.find("value")))
                    continue
                if option_type == "timeoffset":
                    self.option_time_offset(option, origin, name, display, description,
                                            interval.Interval(option.find("value")))
                    continue
                if option_type == "dynamicstring":
                    self.option_dynamic_string(option, origin, name, display, description,
                                               option.find("value").text)
                    continue
                if option_type == "dynamicboolean":
                    self.option_dynamic_boolean(option, origin, name, display, description,
                                        option.find("value").text == "true")
                    continue
                if option_type == "dynamicdouble":
                    self.option_dynamic_double(option, origin, name, display, description,
                                       _to_float(option.find("value")))
                    continue
                if option_type == "dynamicinteger":
                    self.option_dynamic_integer(option, origin, name, display, description,
                                        _to_integer(option.find("value")))
                    continue
                if option_type == "dynamiccalibration":
                    self.option_dynamic_calibration(option, origin, name, display, description,
                                            _unroll_list(option.findall("coefficients/coefficient"), _to_float))
                    continue
                if option_type == "smoother":
                    self.option_smoother(option, origin, name, display, description)
                    continue
                if option_type == "ssl":
                    self.option_ssl(option, origin, name, display, description,
                                    option.find("certificate").text,
                                    option.find("key").text)
                    continue

                self.option_generic(option, origin, name, display, description)

        def run(self):
            self.options.sort(key=lambda r: (int(r.get("sort", default=0)),
                                             _get_origin(r, self.parent.options).
                                             find("name").text))

            self.begin()
            self.run_options()
            self.end()

        def option_generic(self, option, origin, name, display, description):
            self.write(" * --%s\n" % name)

        def option_boolean(self, option, origin, name, display, description, value):
            self.option_generic(option, origin, name, display, description)

        def option_file(self, option, origin, name, display, description, value):
            self.option_string(option, origin, name, display, description, value)

        def option_directory(self, option, origin, name, display, description, path):
            self.option_string(option, origin, name, display, description, path)

        def option_script(self, option, origin, name, display, description, code):
            self.option_string(option, origin, name, display, description, code)

        def option_string(self, option, origin, name, display, description, value):
            self.option_generic(option, origin, name, display, description)

        def option_double(self, option, origin, name, display, description, value):
            self.option_generic(option, origin, name, display, description)

        def option_double_set(self, option, origin, name, display, description, values):
            self.option_generic(option, origin, name, display, description)

        def option_double_list(self, option, origin, name, display, description, values):
            self.option_double_set(option, origin, name, display, description, values)

        def option_integer(self, option, origin, name, display, description, value):
            self.option_generic(option, origin, name, display, description)

        def option_instrument(self, option, origin, name, display, description, suffixes):
            self.option_string_set(option, origin, name, display, description, suffixes)

        def option_string_set(self, option, origin, name, display, description, values):
            self.option_generic(option, origin, name, display, description)

        def option_time(self, option, origin, name, display, description, point):
            self.option_generic(option, origin, name, display, description)

        def option_calibration(self, option, origin, name, display, description, coefficients):
            self.option_generic(option, origin, name, display, description)

        def option_variable(self, option, origin, name, display, description, unit):
            self.option_generic(option, origin, name, display, description)

        def option_input(self, option, origin, name, display, description, input):
            self.option_generic(option, origin, name, display, description)

        def option_operate(self, option, origin, name, display, description, unit):
            self.option_generic(option, origin, name, display, description)

        def option_enum(self, option, origin, name, display, description, selected):
            self.option_generic(option, origin, name, display, description)

        def option_interval(self, option, origin, name, display, description, selected):
            self.option_generic(option, origin, name, display, description)

        def option_interval_block(self, option, origin, name, display, description, selected):
            self.option_interval(option, origin, name, display, description, selected)

        def option_time_offset(self, option, origin, name, display, description, offset):
            self.option_interval(option, origin, name, display, description, offset)

        def option_dynamic_string(self, option, origin, name, display, description, value):
            self.option_string(option, origin, name, display, description, value)

        def option_dynamic_boolean(self, option, origin, name, display, description, value):
            self.option_boolean(option, origin, name, display, description, value)

        def option_dynamic_double(self, option, origin, name, display, description, value):
            self.option_double(option, origin, name, display, description, value)

        def option_dynamic_integer(self, option, origin, name, display, description, value):
            self.option_integer(option, origin, name, display, description, value)

        def option_dynamic_calibration(self, option, origin, name, display, description, coefficients):
            self.option_calibration(option, origin, name, display, description, coefficients)

        def option_smoother(self, option, origin, name, display, description):
            self.option_generic(option, origin, name, display, description)

        def option_ssl(self, option, origin, name, display, description, certificate, key):
            self.option_generic(option, origin, name, display, description)

    def make_example(self, example):
        return Examples.Example(example)

    def run(self):
        for example in self.examples:
            e = self.make_example(example)
            if isinstance(e, list):
                for r in e:
                    r.parent = self
                    r.run()
            else:
                e.parent = self
                e.run()

           

    
