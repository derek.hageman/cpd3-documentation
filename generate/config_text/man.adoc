= DA.CONFIG.TEXT(1)
:man source:  CPD3
:man version: {revnumber}
:man manual:  CPD3 Manual


== NAME

da.config.text - Flat text file configuration display


== SYNOPSIS

*da.config.text* [_OPTIONS_...] [station] variables [times] [archive]


== DESCRIPTION

This program provides an interface to display or edit configuration information as text files.
This generally allows for a flat file representation similar to a classif configuration file.

== OPTIONS

ifdef::cpd3-final[]

include::options_man.adoc[]

endif::cpd3-final[]


== ARGUMENTS

ifdef::cpd3-final[]

*station*::
+
--
include::{cpd3-shared-directory}/man_station.adoc[]
--
*variables*::
+
--
include::{cpd3-shared-directory}/man_variables.adoc[]
--
*times*::
+
--
include::{cpd3-shared-directory}/man_times.adoc[]
--
*archive*::
+
--
include::{cpd3-shared-directory}/man_archive.adoc[]
--

endif::cpd3-final[]


If the mode is set to read data from standard input, no bare word arguments are required.


ifdef::cpd3-final[]

include::{cpd3-shared-directory}/man_tail.adoc[]

endif::cpd3-final[]

