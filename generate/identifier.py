#!/usr/bin/python3

import xml.etree.ElementTree

def to_type(input):
    type = input.find('identifier/type')
    if type is not None:
        type = type.text.lower()
    return type


def to_id(input):
    type = to_type(input)

    if type == "filter" or type == "read" or type == "external" or type == "input" or type == "action" or type == "archive":
        component = input.find("identifier/component").text
        id = component.strip()
    elif type == "display":
        component = input.find("identifier/output").text
        if input.find("options/file/[@id='file']") is not None:
            id = "plot_" + component.strip()
        else:
            id = "show_" + component.strip()
    else:
        name = input.find("command/program")
        if name is not None:
            name = name.text.strip()
        else:
            raise Exception("No command available")
        id = name.replace(".", "_").lower()

    return id


def to_cli_file(input):
    type = to_type(input)

    if type == "filter" or type == "read" or type == "external" or type == "input" or type == "action" or type == "archive":
        component = input.find("identifier/component").text
        name = "da_" + component.strip().lower()
    elif type == "display":
        component = input.find("identifier/output").text
        if input.find("options/file/[@id='file']") is not None:
            name = "da_plot_" + component.strip()
        else:
            name = "da_show_" + component.strip()
    else:
        name = input.find("command/program")
        if name is not None:
            name = name.text.strip()
        else:
            raise Exception("No command available")
        name = name.replace(".", "_").lower()

    return name