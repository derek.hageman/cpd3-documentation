#!/usr/bin/python3

import interval

def format_double(input):
    if int(input) == input:
        return str(int(input))
    return str(input)

def _unroll_string_list(list):
    result = []
    for e in list:
        result.append(e.text)
    return result


def _is_set(input, path=None):
    if input is None:
        return False
    if path is None:
        return True
    return input.find(path) is not None


def _to_float(input):
    if input is None or input.text is None:
        return None
    return float(input.text)


def _to_integer(input):
    if input is None or input.text is None:
        return None
    return int(input.text)


class Options:
    def __init__(self, list, output):
        self.options = list
        self._output = output

    def write(self, data):
        self._output.write(data)

    def run(self):
        self.options.sort(key=lambda r: (int(r.get("sort", default=0)), r.find("name").text))

        for option in self.options:
            option_type = option.tag.lower()

            name = option.find("name").text
            display = option.find("display").text
            description = option.find("description").text
            default = option.find("default")
            if default is not None:
                default = default.text

            if option_type == "boolean":
                self.option_boolean(option, name, display, description, default)
                continue
            if option_type == "file":
                self.option_file(option, name, display, description, default,
                                 _unroll_string_list(option.findall("extensions/extension")),
                                 _is_set(option, "read"),
                                 _is_set(option, "write"))
                continue
            if option_type == "directory":
                self.option_directory(option, name, display, description, default)
                continue
            if option_type == "script":
                self.option_script(option, name, display, description, default)
                continue
            if option_type == "variable":
                self.option_variable(option, name, display, description, default)
                continue
            if option_type == "string":
                self.option_string(option, name, display, description, default)
                continue
            if option_type == "double":
                self.option_double(option, name, display, description, default,
                                   _is_set(option, "limits/[@undefined='true']"),
                                   _to_float(option.find("limits/minimum")),
                                   _to_float(option.find("limits/maximum")),
                                   _is_set(option, "limits/minimum/[@exclusive='true']"),
                                   _is_set(option, "limits/maximum/[@exclusive='true']"))
                continue
            if option_type == "doubleset":
                self.option_double_set(option, name, display, description, default,
                                       _to_float(option.find("limits/minimum")),
                                       _to_float(option.find("limits/maximum")),
                                       _is_set(option, "limits/minimum/[@exclusive='true']"),
                                       _is_set(option, "limits/maximum/[@exclusive='true']"))
                continue
            if option_type == "doublelist":
                self.option_double_list(option, name, display, description, default,
                                        _to_integer(option.find("components")))
                continue
            if option_type == "integer":
                self.option_integer(option, name, display, description, default,
                                    _is_set(option, "limits/[@undefined='true']"),
                                    _to_integer(option.find("limits/minimum")),
                                    _to_integer(option.find("limits/maximum")))
                continue
            if option_type == "instrument":
                self.option_instrument(option, name, display, description, default)
                continue
            if option_type == "stringset":
                self.option_string_set(option, name, display, description, default)
                continue
            if option_type == "time":
                self.option_time(option, name, display, description, default,
                                 _is_set(option, "undefined"))
                continue
            if option_type == "calibration":
                self.option_calibration(option, name, display, description, default,
                                        _is_set(option, "invalid"), _is_set(option, "constant"))
                continue
            if option_type == "input":
                self.option_input(option, name, display, description, default)
                continue
            if option_type == "operate":
                self.option_operate(option, name, display, description, default)
                continue
            if option_type == "enum":
                values = []
                lookup = dict()
                for v in option.findall("values/value"):
                    id = v.get("id")
                    add = dict()
                    add["id"] = id
                    add["name"] = v.find("name").text
                    add["description"] = v.find("description").text
                    values.append(add)
                    lookup[id] = add
                for v in option.findall("aliases/alias"):
                    origin = v.find("origin")
                    id = None
                    if origin is not None:
                        id = origin.get("id")
                    add = dict()
                    add["name"] = v.find("name").text
                    if id is not None and id in lookup:
                        add["origin"] = lookup[id]
                    else:
                        add["description"] = v.find("description").text
                    values.append(add)
                values.sort(key=lambda r: r["name"])
                self.option_enum(option, name, display, description, default, values)
                continue
            if option_type == "interval":
                self.option_interval(option, name, display, description, default,
                                     _is_set(option, "undefined"),
                                     _is_set(option, "zero"),
                                     _is_set(option, "negative"),
                                     interval.Interval(option.find("fallback")))
                continue
            if option_type == "intervalblock":
                self.option_interval_block(option, name, display, description, default,
                                           interval.Interval(option.find("fallback")))
                continue
            if option_type == "timeoffset":
                self.option_time_offset(option, name, display, description, default,
                                        _is_set(option, "zero"),
                                        _is_set(option, "negative"),
                                        interval.Interval(option.find("fallback")))
                continue
            if option_type == "dynamicstring":
                self.option_dynamic_string(option, name, display, description, default)
                continue
            if option_type == "dynamicboolean":
                self.option_dynamic_boolean(option, name, display, description, default)
                continue
            if option_type == "dynamicdouble":
                self.option_dynamic_double(option, name, display, description, default,
                                           _is_set(option, "limits/[@undefined='true']"),
                                           _to_float(option.find("limits/minimum")),
                                           _to_float(option.find("limits/maximum")),
                                           _is_set(option, "limits/minimum/[@exclusive='true']"),
                                           _is_set(option, "limits/maximum/[@exclusive='true']"))
                continue
            if option_type == "dynamicinteger":
                self.option_dynamic_integer(option, name, display, description, default,
                                            _is_set(option, "limits/[@undefined='true']"),
                                            _to_integer(option.find("limits/minimum")),
                                            _to_integer(option.find("limits/maximum")))
                continue
            if option_type == "dynamiccalibration":
                self.option_dynamic_calibration(option, name, display, description, default)
                continue
            if option_type == "smoother":
                self.option_smoother(option, name, display, description, default,
                                     _is_set(option, "stability"),
                                     _is_set(option, "spike"))
                continue
            if option_type == "ssl":
                self.option_ssl(option, name, display, description, default)
                continue

            self.option_generic(option, name, display, description, default)

    def begin_generic(self, option, name, display, description, default):
        self.write("%s - %s::\n" % (name, display))

    def body_generic(self, option, name, display, description, default):
        self.write("+\n")
        self.write("--\n")
        self.write("%s\n" % description.strip())
        if default is not None:
            self.write("\n")
            self.write("Default: *%s*\n" % default.strip())
        self.write("--\n")

    def option_generic(self, option, name, display, description, default):
        self.begin_generic(option, name, display, description, default)
        self.body_generic(option, name, display, description, default)

    def option_boolean(self, option, name, display, description, default):
        self.option_generic(option, name, display, description, default)

    def option_file(self, option, name, display, description, default, extensions, read, write):
        self.option_string(option, name, display, description, default)

    def option_directory(self, option, name, display, description, default):
        self.option_string(option, name, display, description, default)

    def option_script(self, option, name, display, description, default):
        self.option_string(option, name, display, description, default)

    def option_variable(self, option, name, display, description, default):
        self.option_string(option, name, display, description, default)

    def option_string(self, option, name, display, description, default):
        self.option_generic(option, name, display, description, default)

    def option_double(self, option, name, display, description, default, undefined, minimum,
                      maximum, minimum_exclusive, maximum_exclusive):
        self.option_generic(option, name, display, description, default)

    def option_double_set(self, option, name, display, description, default, minimum, maximum,
                          minimum_exclusive, maximum_exclusive):
        self.option_generic(option, name, display, description, default)

    def option_double_list(self, option, name, display, description, default, minimum_elements):
        self.option_generic(option, name, display, description, default)

    def option_integer(self, option, name, display, description, default, undefined, minimum,
                       maximum):
        self.option_generic(option, name, display, description, default)

    def option_instrument(self, option, name, display, description, default):
        self.option_string_set(option, name, display, description, default)

    def option_string_set(self, option, name, display, description, default):
        self.option_generic(option, name, display, description, default)

    def option_time(self, option, name, display, description, default, undefined):
        self.option_generic(option, name, display, description, default)

    def option_calibration(self, option, name, display, description, default, invalid, constant):
        self.option_generic(option, name, display, description, default)

    def option_input(self, option, name, display, description, default):
        self.option_generic(option, name, display, description, default)

    def option_operate(self, option, name, display, description, default):
        self.option_generic(option, name, display, description, default)

    def option_enum(self, option, name, display, description, default, values):
        self.option_generic(option, name, display, description, default)

    def option_interval(self, option, name, display, description, default, undefined, zero,
                        negative, default_interval):
        self.option_generic(option, name, display, description, default)

    def option_interval_block(self, option, name, display, description, default, default_interval):
        self.option_generic(option, name, display, description, default)

    def option_time_offset(self, option, name, display, description, default, zero,
                           negative, default_interval):
        self.option_generic(option, name, display, description, default)

    def option_dynamic_string(self, option, name, display, description, default):
        self.option_generic(option, name, display, description, default)

    def option_dynamic_boolean(self, option, name, display, description, default):
        self.option_generic(option, name, display, description, default)

    def option_dynamic_double(self, option, name, display, description, default, undefined, minimum,
                              maximum, minimum_exclusive, maximum_exclusive):
        self.option_generic(option, name, display, description, default)

    def option_dynamic_integer(self, option, name, display, description, default, undefined,
                               minimum, maximum):
        self.option_generic(option, name, display, description, default)

    def option_dynamic_calibration(self, option, name, display, description, default):
        self.option_generic(option, name, display, description, default)

    def option_smoother(self, option, name, display, description, default, stability, spike):
        self.option_generic(option, name, display, description, default)

    def option_ssl(self, option, name, display, description, default):
        self.option_generic(option, name, display, description, default)
