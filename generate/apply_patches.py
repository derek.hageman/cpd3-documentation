#!/usr/bin/python3

import sys
import subprocess
import tempfile

input = sys.argv[1]
output = sys.argv[2]
patches = sys.argv[3:]

if len(patches) <= 0:
    subprocess.check_call(["cp", input, output])
    exit(0)

input = open(input, "r")
for i in range(0,len(patches)):
    if i == len(patches)-1:
        target = open(output, "w")
    else:
        target = tempfile.TemporaryFile()
    subprocess.check_call(["patch", "-o", target.name, "-i", patches[i], input.name])
    input.close()
    input = target
    input.seek(0)