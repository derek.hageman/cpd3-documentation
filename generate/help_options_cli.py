#!/usr/bin/python3

import sys
import xml.etree.ElementTree
import options
import xmlloader

input = xmlloader.load(sys.argv[1], sys.argv[3:])
output = open(sys.argv[2], "w", encoding="utf-8")


class _Options(options.Options):

    def begin_generic(self, option, name, display, description, default):
        self.write("--%s::\n" % name)

    def option_boolean(self, option, name, display, description, default):
        self.write("--%s[=<<{cli-option-link-boolean},BOOLEAN>>]::\n" % name)
        self.body_generic(option, name, display, description, default)

    def option_file(self, option, name, display, description, default, extensions, read, write):
        self.write("--%s=<<{cli-option-link-file},FILE>>::\n" % name)
        self.body_generic(option, name, display, description, default)

    def option_directory(self, option, name, display, description, default):
        self.write("--%s=<<{cli-option-link-directory},DIRECTORY>>::\n" % name)
        self.body_generic(option, name, display, description, default)

    def option_script(self, option, name, display, description, default):
        self.write("--%s=<<{cli-option-link-script},SCRIPT>>::\n" % name)
        self.body_generic(option, name, display, description, default)

    def option_string(self, option, name, display, description, default):
        self.write("--%s=<<{cli-option-link-string},STRING>>::\n" % name)
        self.body_generic(option, name, display, description, default)

    def _double_range(self, undefined, minimum, maximum, minimum_exclusive, maximum_exclusive):
        self.write("+\n")
        self.write("--\n")
        if minimum is not None:
            if minimum_exclusive:
                lower = "strictly greater than %s" % options.format_double(minimum)
            else:
                lower = "greater than or equal to %s" % options.format_double(minimum)

            if maximum is not None:
                if maximum_exclusive:
                    upper = "strictly less than %s" % options.format_double(maximum)
                else:
                    upper = "less than or equal to %s" % options.format_double(maximum)
                self.write("This option only accepts numbers %s and %s.\n" % (lower, upper))
            else:
                self.write("This option only accepts numbers %s.\n" % lower)
        elif maximum is not None:
            if maximum_exclusive:
                upper = "strictly less than %s" % options.format_double(maximum)
            else:
                upper = "less than or equal to %s" % options.format_double(maximum)
            self.write("This option only accepts numbers %s.\n" % upper)
        if undefined:
            self.write("Undefined values are permitted, to indicate that the option is set but not to a specific value.\n")
        self.write("--\n")

    def option_double(self, option, name, display, description, default, undefined, minimum,
                      maximum, minimum_exclusive, maximum_exclusive):
        self.write("--%s=<<{cli-option-link-double},NUMBER>>::\n" % name)
        self.body_generic(option, name, display, description, default)
        self._double_range(undefined, minimum, maximum, minimum_exclusive, maximum_exclusive)

    def option_double_set(self, option, name, display, description, default, minimum, maximum,
                          minimum_exclusive, maximum_exclusive):
        self.write("--%s=<<{cli-option-link-doubleset},NUMBER...>>::\n" % name)
        self.body_generic(option, name, display, description, default)
        self._double_range(False, minimum, maximum, minimum_exclusive, maximum_exclusive)

    def option_double_list(self, option, name, display, description, default, minimum_elements):
        self.write("--%s=<<{cli-option-link-doublelist},NUMBER...>>::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.write("+\n")
        self.write("--\n")
        if minimum_elements is not None:
            if minimum_elements > 1:
                self.write("This option requires at least %d numbers if it is used.\n" % minimum_elements)
            else:
                self.write("This option requires at least one number if it is used.\n")
        else:
            self.write("This option may also be set to an empty value to indicate an empty list of numbers.\n")
        self.write("--\n")

    def _integer_range(self, undefined, minimum, maximum):
        self.write("+\n")
        self.write("--\n")
        if minimum is not None:
            if maximum is not None:
                self.write("This option only accepts integers greater than or equal to %d and less than or equal to %d.\n" %
                           (minimum, maximum))
            else:
                self.write("This option only accepts integers greater than or equal to %d.\n" % minimum)
        elif maximum is not None:
            self.write("This option only accepts integers less than or equal to %d.\n" % maximum)
        if undefined:
            self.write("Undefined values are permitted, to indicate that the option is set but not to a specific value.\n")
        self.write("--\n")

    def option_integer(self, option, name, display, description, default, undefined, minimum,
                       maximum):
        self.write("--%s=<<{cli-option-link-integer},INTEGER>>::\n" % name)
        self.body_generic(option, name, display, description, default)
        self._integer_range(undefined, minimum, maximum)

    def option_instrument(self, option, name, display, description, default):
        self.write("--%s=<<{cli-option-link-instrument},SUFFIX...>>::\n" % name)
        self.body_generic(option, name, display, description, default)

    def option_string_set(self, option, name, display, description, default):
        self.write("--%s=<<{cli-option-link-instrument},STRING...>>::\n" % name)
        self.body_generic(option, name, display, description, default)

    def option_time(self, option, name, display, description, default, undefined):
        self.write("--%s=<<{cli-option-link-time},TIME>>::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.write("+\n")
        self.write("--\n")
        if undefined:
            self.write("Undefined values are permitted, to indicate that the option is set but not to a specific time.\n")
        self.write("--\n")

    def option_calibration(self, option, name, display, description, default, invalid, constant):
        self.write("--%s=<<{cli-option-link-calibration},COEFFICIENT...>>::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.write("+\n")
        self.write("--\n")
        if invalid:
            if constant:
                self.write("Invalid (empty) as well as constant values are accepted.\n")
            else:
                self.write("Invalid (empty) calibrations are accepted.\n")
        elif constant:
            self.write("Constant values are accepted.\n")
        self.write("--\n")

    def option_variable(self, option, name, display, description, default):
        self.write("--%s=<<{cli-option-link-variable},VARIABLE>>::\n" % name)
        self.body_generic(option, name, display, description, default)

    def option_input(self, option, name, display, description, default):
        self.write("--%s=<<{cli-option-link-input},INPUT>>::\n" % name)
        self.body_generic(option, name, display, description, default)

    def option_operate(self, option, name, display, description, default):
        self.write("--%s=<<{cli-option-link-operate},SELECTION>>::\n" % name)
        self.body_generic(option, name, display, description, default)

    def option_enum(self, option, name, display, description, default, values):
        self.write("--%s=<<{cli-option-link-enum},TYPE>>::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.write("+\n")
        self.write("--\n")
        if len(values) > 0:
            self.write("The possible values are:\n")
            self.write("\n")
            for v in values:
                self.write("--%s=*%s*;;\n" % (name, v["name"]))
                if "origin" in v:
                    self.write("Same as --%s=*%s*" % (name, v["origin"]["name"]))
                else:
                    self.write(v["description"])
                self.write("\n")
        self.write("--\n")

    def option_interval(self, option, name, display, description, default, undefined, zero,
                        negative, default_interval):
        self.write("--%s=<<{cli-option-link-interval},INTERVAL>>::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.write("+\n")
        self.write("--\n")
        if default_interval is not None and default_interval.unit is not None:
            self.write("When no units are specified the interval is assumed to be in %s.\n" % default_interval.unit.printable(0))
        if undefined:
            self.write("Undefined intervals are accepted.\n")
        if not zero:
            if not negative:
                self.write("The interval must be greater than zero in length.\n")
            else:
                self.write("The interval cannot be exactly zero in length.\n")
        elif not negative:
            self.write("The interval must be greater than or equal to zero length.\n")
        self.write("--\n")

    def option_interval_block(self, option, name, display, description, default, default_interval):
        self.write("--%s=<<{cli-option-link-intervalblock},INTERVALBLOCK>>::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.write("+\n")
        self.write("--\n")
        if default_interval is not None and default_interval.unit is not None:
            self.write("When no units are specified the interval is assumed to be in %s.\n" % default_interval.unit.printable(0))
        self.write("--\n")

    def option_time_offset(self, option, name, display, description, default, zero,
                           negative, default_interval):
        self.write("--%s=<<{cli-option-link-timeoffset},OFFSET>>::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.write("+\n")
        self.write("--\n")
        if default_interval is not None and default_interval.unit is not None:
            self.write("When no units are specified the offset is assumed to be in %s.\n" % default_interval.unit.printable(0))
        if default_interval is not None and default_interval.aligned is not None:
            self.write("Unless explicitly disabled, the offset is assumed to be aligned.\n")
        if not zero:
            if not negative:
                self.write("The interval must be greater than zero in length.\n")
            else:
                self.write("The interval cannot be exactly zero in length.\n")
        elif not negative:
            self.write("The interval must be greater than or equal to zero length.\n")
        self.write("--\n")

    def option_dynamic_string(self, option, name, display, description, default):
        self.write("--%s=<<{cli-option-link-dynamicstring},STRINGS>>::\n" % name)
        self.body_generic(option, name, display, description, default)

    def option_dynamic_boolean(self, option, name, display, description, default):
        self.write("--%s[=<<{cli-option-link-dynamicboolean},BOOLEANS>>]::\n" % name)
        self.body_generic(option, name, display, description, default)

    def option_dynamic_double(self, option, name, display, description, default, undefined, minimum,
                              maximum, minimum_exclusive, maximum_exclusive):
        self.write("--%s=<<{cli-option-link-dynamicdouble},NUMBERS>>::\n" % name)
        self.body_generic(option, name, display, description, default)
        self._double_range(undefined, minimum, maximum, minimum_exclusive, maximum_exclusive)

    def option_dynamic_integer(self, option, name, display, description, default, undefined,
                               minimum, maximum):
        self.write("--%s=<<{cli-option-link-dynamicinteger},INTEGER>>::\n" % name)
        self.body_generic(option, name, display, description, default)
        self._integer_range(undefined, minimum, maximum)

    def option_dynamic_calibration(self, option, name, display, description, default):
        self.write("--%s=<<{cli-option-link-dynamiccalibration},COEFFICIENTS...>>::\n" % name)
        self.body_generic(option, name, display, description, default)

    def option_smoother(self, option, name, display, description, default, stability, spike):
        self.write("--%s=<<{cli-option-link-smoother},SMOOTHER>>::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.write("+\n")
        self.write("--\n")
        if stability:
            if spike:
                self.write("The smoother is used for stability and spike detection.\n")
            else:
                self.write("The smoother is used for stability detection.\n")
        elif spike:
            self.write("The smoother is used for spike detection.\n")
        self.write("\n")
        self.write("--\n")

    def option_ssl(self, option, name, display, description, default):
        self.write("--%s=<<{cli-option-link-ssl},KEY,CERTIFICATE>>::\n" % name)
        self.body_generic(option, name, display, description, default)


_Options(input.findall('options/*'), output).run()
