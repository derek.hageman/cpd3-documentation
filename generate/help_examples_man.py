#!/usr/bin/python3

import sys
import xml.etree.ElementTree
import examples
import xmlloader
import identifier
import dataunit

input = xmlloader.load(sys.argv[1], sys.argv[3:])
output = open(sys.argv[2], "w", encoding="utf-8")

htype = identifier.to_type(input)

if htype == "filter" or htype == "read" or htype == "external" or htype == "input" or htype == "action" or htype == "archive":
    component = input.find("identifier/component").text
    command = "da." + component.replace("_", ".").lower()
elif htype == "display":
    component = input.find("identifier/output").text
    if input.find("options/file/[@id='file']") is not None:
        command = "da.plot." + component.replace("_", ".").lower()
    else:
        command = "da.show." + component.replace("_", ".").lower()
else:
    name = input.find("command/program")
    if name is not None:
        command = name.text.strip()
    else:
        raise Exception("No command available")

class _Examples(examples.Examples):
    def __init__(self, list, options, output):
        self._first = True
        examples.Examples.__init__(self, list, options, output)

    class _Example(examples.Examples.Example):
        def __init__(self, example, first):
            examples.Examples.Example.__init__(self, example)
            self._first = first
            self._arguments = []

        def begin(self):
            pass

        def end(self):
            self.write("%s::\n" % self.title)
            if self.description is not None:
                self.write("+\n")
                self.write("--\n")
                self.write("%s\n" % self.description.strip())
                self.write("--\n")
            self.write("+\n")
            self.write("----\n")
            self.write(command)
            if len(self._arguments) > 0:
                self.write(" ")
                self.write(" ".join(self._arguments))

            write_station = False
            write_variable = False
            write_times = False
            write_archive = False

            if htype == "filter" or htype == "display":
                write_station = True
                write_variable = True
                write_times = True
                write_archive = True
            elif htype == "action":
                write_station = input.find("identifier/allowedStations") is not None
                write_times = input.find("identifier/time") is not None

            if self.file:
                self.write(" input.dat")
                write_station = False
                write_variable = False
                write_times = False
                write_archive = False
            if write_station:
                if self.station is None or len(self.station) == 0:
                    self.write(" allstations")
                else:
                    self.write(" %s" % self.station)
            if write_variable:
                inputs = [ ]
                if self.scattering:
                    inputs.append("S11a")
                if self.absorption:
                    inputs.append("A11a")
                if self.counts:
                    inputs.append("N61a")
                for a in self.auxiliary:
                    inputs.append(a)
                if len(inputs) == 0:
                    inputs.append("everything")
                self.write(" %s" % (",").join(inputs))
            if write_times:
                if self.time_start is None:
                    if self.time_end is None:
                        self.write(" forever")
                    else:
                        self.write(" none %s" % examples.format_time(self.time_end))
                else:
                    self.write(" %s" % examples.format_time(self.time_start))
                    if self.time_end is None:
                        self.write(" none")
                    else:
                        self.write(" %s" % examples.format_time(self.time_end))
            if write_archive:
                if self.archive != "raw":
                    if self.archive is None or len(self.archive) == 0:
                        self.write(" allarchives")
                    else:
                        self.write(" %s" % self.archive)
            self.write("\n")

            self.write("----\n")

            if not self._first or htype != "filter":
                return
            if self.file:
                return

            self.write("+\n")
            self.write("Or:\n")
            self.write("+\n")
            self.write("----\n")
            self.write(command)
            if len(self._arguments) > 0:
                self.write(" ")
                self.write(" ".join(self._arguments))
            self.write(" input_file.c3d\n")
            self.write("----\n")

            self.write("+\n")
            self.write("Or:\n")
            self.write("+\n")
            self.write("----\n")
            self.write("da.get")
            if write_station:
                if self.station is None or len(self.station) == 0:
                    self.write(" allstations")
                else:
                    self.write(" %s" % self.station)
            if write_variable:
                inputs = [ ]
                if self.scattering:
                    inputs.append("S11a")
                if self.absorption:
                    inputs.append("A11a")
                if self.counts:
                    inputs.append("N61a")
                for a in self.auxiliary:
                    inputs.append(a)
                if len(inputs) == 0:
                    inputs.append("everything")
                self.write(" %s" % (",").join(inputs))
            if write_times:
                if self.time_start is None:
                    if self.time_end is None:
                        self.write(" forever")
                    else:
                        self.write(" none %s" % examples.format_time(self.time_end))
                else:
                    self.write(" %s" % examples.format_time(self.time_start))
                    if self.time_end is None:
                        self.write(" none")
                    else:
                        self.write(" %s" % examples.format_time(self.time_end))
            if write_archive:
                if self.archive != "raw":
                    if self.archive is None or len(self.archive) == 0:
                        self.write(" allarchives")
                    else:
                        self.write(" %s" % self.archive)
            self.write(" | ")
            self.write(command)
            if len(self._arguments) > 0:
                self.write(" ")
                self.write(" ".join(self._arguments))
            self.write("\n")
            self.write("----\n")

        def option_generic(self, option, origin, name, display, description):
            self._arguments.append("--%s" % name)

        def option_boolean(self, option, origin, name, display, description, value):
            if value:
                self._arguments.append("--%s" % name)
            else:
                self._arguments.append("--%s=false" % name)

        def option_string(self, option, origin, name, display, description, value):
            if ' ' in value or ';' in value:
                self._arguments.append("--%s='%s'" % (name, value))
            else:
               self._arguments.append("--%s=%s" % (name, value))

        def option_double(self, option, origin, name, display, description, value):
            self._arguments.append("--%s=%s" % (name, examples.format_double(value)))

        def option_double_set(self, option, origin, name, display, description, values):
            for i, v in enumerate(values):
                values[i] = examples.format_double(v)
            self._arguments.append("--%s=%s" % (name, ",".join(values)))

        def option_integer(self, option, origin, name, display, description, value):
            self._arguments.append("--%s=%d" % (name, value))

        def option_string_set(self, option, origin, name, display, description, values):
            self._arguments.append("--%s=%s" % (name, ",".join(values)))

        def option_time(self, option, origin, name, display, description, point):
            if point is None:
                self._arguments.append("--%s=undef" % name)
            else:
                self._arguments.append("--%s=%s" % (name, examples.format_time(point)))

        def option_calibration(self, option, origin, name, display, description, coefficients):
            for i, v in enumerate(coefficients):
                coefficients[i] = examples.format_double(v)
            self._arguments.append("--%s=%s" % (name, ",".join(coefficients)))

        def option_input(self, option, origin, name, display, description, input):
            if type(input) is dataunit.DataUnit:
                self._arguments.append("--%s=%s" % (name, ":".join(input.components())))
            else:
                self._arguments.append("--%s=%s" % (name, examples.format_double(input)))

        def option_operate(self, option, origin, name, display, description, unit):
            self._arguments.append("--%s=%s" % (name, ":".join(unit.components())))

        def option_enum(self, option, origin, name, display, description, selected):
            self._arguments.append("--%s=%s" % (name, selected["name"]))

        def option_interval(self, option, origin, name, display, description, selected):
            self._arguments.append("--%s=%s" % (name, selected.format()))

        def option_dynamic_calibration(self, option, origin, name, display, description,
                                       coefficients):
            for i, v in enumerate(coefficients):
                coefficients[i] = examples.format_double(v)
            self._arguments.append("--%s=%s" % (name, ":".join(coefficients)))

        def option_smoother(self, option, origin, name, display, description):
            self.option_generic(option, origin, name, display, description)

        def option_ssl(self, option, origin, name, display, description, certificate, key):
            if certificate is None:
                certificate = ""
            if key is None:
                key = ""
            self._arguments.append("--%s=%s,s%s" % (name, key, certificate))


    def make_example(self, example):
        isFirst = self._first
        self._first = False
        return _Examples._Example(example, isFirst)


_Examples(input.findall('examples/*'), input.find('options'), output).run()
