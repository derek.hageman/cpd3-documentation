#!/usr/bin/python3

import xml.etree.ElementTree
import subprocess
import tempfile

def load(input, patches):
    if len(patches) <= 0:
        input = xml.etree.ElementTree.parse(input)
    else:
        patches.sort()
        input = open(input, "r")
        for p in patches:
            t = tempfile.TemporaryFile()
            subprocess.check_call([p], stdin=input, stdout=t)
            t.seek(0)
            input.close()
            input = t
        t = input
        input = xml.etree.ElementTree.parse(t)
        t.close()
    return input