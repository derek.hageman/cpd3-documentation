#!/usr/bin/python3

import sys
import xml.etree.ElementTree
import options
import xmlloader

input = xmlloader.load(sys.argv[1], sys.argv[3:])
output = open(sys.argv[2], "w", encoding="utf-8")

class _Options(options.Options):

    def begin_generic(self, option, name, display, description, default):
        self.write("*--%s*::\n" % name)

    def generic_context(self, name, id, end=True):
        self.write("+\n")
        self.write("--\n")
        self.write("{set:option-name:%s}\n" % name)
        self.write("include::{cli-option-directory}/man_%s.adoc[]\n" % id)
        if end:
            self.write("--\n")

    def option_boolean(self, option, name, display, description, default):
        self.write("*--%s*[=BOOLEAN]::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.generic_context(name, "boolean")

    def option_file(self, option, name, display, description, default, extensions, read, write):
        self.write("*--%s*=FILE::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.generic_context(name, "file")

    def option_directory(self, option, name, display, description, default):
        self.write("*--%s*=DIRECTORY::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.generic_context(name, "directory")

    def option_script(self, option, name, display, description, default):
        self.write("*--%s*=SCRIPT::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.generic_context(name, "script")

    def option_string(self, option, name, display, description, default):
        self.write("*--%s*=STRING::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.generic_context(name, "string")

    def _double_range(self, name, undefined, minimum, maximum, minimum_exclusive, maximum_exclusive):
        if minimum is not None:
            if minimum_exclusive:
                lower = "strictly greater than %s" % options.format_double(minimum)
            else:
                lower = "greater than or equal to %s" % options.format_double(minimum)
            if maximum is not None:
                if maximum_exclusive:
                    upper = "strictly less than %s" % options.format_double(maximum)
                else:
                    upper = "less than or equal to %s" % options.format_double(maximum)

                self.write("The value must br %s and %s.\n" % (lower, upper))
                if minimum+1 < maximum:
                    self.write("For example, use _--%s=%s_ to set the value to one greater than its minimum.\n" % (name, options.format_double(minimum+1)))
                else:
                    self.write("For example, use _--%s=%d_ to set the value to its midpoint.\n" % (name, (maximum+minimum)/2))
            else:
                self.write("The value must be %s.\n" % lower)
                self.write("For example, use _--%s=%s_ to set the value to one greater than its minimum.\n" % (name, options.format_double(minimum+1)))
        elif maximum is not None:
            if maximum_exclusive:
                upper = "strictly less than %s" % options.format_double(maximum)
            else:
                upper = "less than or equal to %s" % options.format_double(maximum)
            self.write("The value must be %s.\n" % upper)
            self.write("For example, use _--%s=%s_ to set the value to one less than its maximum.\n" % (name, options.format_double(maximum-1)))
        else:
            self.write("For example, use _--%s=0_ to set the value to zero.\n" % name)
        if undefined:
            self.write("The value may also be set to empty, _undef_, _undefined_, _inf_, _infinity_, or _mvc_ to indicate that the option is set but not to a specific value.\n")
            self.write("For example, _--%s=undef_ or _--%s=INFINITY_ both set the option to undefined.\n" % (name, name))

    def option_double(self, option, name, display, description, default, undefined, minimum,
                      maximum, minimum_exclusive, maximum_exclusive):
        self.write("*--%s*=NUMBER::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.generic_context(name, "double", False)
        self._double_range(name, undefined, minimum, maximum, minimum_exclusive, maximum_exclusive)
        self.write("--\n")

    def option_double_set(self, option, name, display, description, default, minimum, maximum,
                          minimum_exclusive, maximum_exclusive):
        self.write("*--%s*=NUMBER...::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.generic_context(name, "doubleset", False)
        self._double_range(name, False, minimum, maximum, minimum_exclusive, maximum_exclusive)
        self.write("--\n")

    def option_double_list(self, option, name, display, description, default, minimum_elements):
        self.write("*--%s*=NUMBER...::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.generic_context(name, "doublelist", False)
        if minimum_elements is not None:
            if minimum_elements > 1:
                self.write("This option requires at least %d numbers if it is present.\n")
            else:
                self.write("This option requires at one number if it is present.\n")
        else:
            self.write("This option may also be set to empty to indicate an empty list.\n")
            self.write("For example _--%s=_ and just _--%s_ both specify an empty list.\n" % (name, name))
        self.write("--\n")

    def _integer_range(self, name, undefined, minimum, maximum):
        if minimum is not None:
            if maximum is not None:
                self.write("The value must be greater than or equal to %d and less than or equal to %d.\n" %
                           (minimum, maximum))
                self.write("For example, use _--%s=%d_ to set the value to its midpoint.\n" % (name, (maximum+minimum)/2))
            else:
                self.write("The value must be greater than or equal to %d.\n" % minimum)
                self.write("For example, use _--%s=%d_ to set the value to its minimum.\n" % (name, minimum))
        elif maximum is not None:
            self.write("The value must be less than or equal to %d.\n" % maximum)
            self.write("For example, use _--%s=%d_ to set the value to its maximum.\n" % (name, maximum))
        else:
            self.write("For example, use _--%s=0_ to set the value to zero.\n" % name)
        if undefined:
            self.write("The value may also be set to empty, _undef_, _undefined_, _inf_, _infinity_, or _mvc_ to indicate that the option is set but not to a specific value.\n")
            self.write("For example, _--%s=undef_ or _--%s=INFINITY_ both set the option to undefined.\n" % (name, name))

    def option_integer(self, option, name, display, description, default, undefined, minimum,
                       maximum):
        self.write("*--%s*=INTEGER::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.generic_context(name, "integer", False)
        self._integer_range(name, undefined, minimum, maximum)
        self.write("--\n")

    def option_instrument(self, option, name, display, description, default):
        self.write("*--%s*=SUFFIX...::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.generic_context(name, "instrument")

    def option_string_set(self, option, name, display, description, default):
        self.write("*--%s*=STRING...::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.generic_context(name, "stringset")

    def option_time(self, option, name, display, description, default, undefined):
        self.write("*--%s*=TIME::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.generic_context(name, "time", False)
        if undefined:
            self.write("The option also accepts undefined times, specified as an infinite value or by setting the option without a value.\n")
        self.write("--\n")

    def option_calibration(self, option, name, display, description, default, invalid, constant):
        self.write("*--%s*=COEFFICIENT...::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.generic_context(name, "calibration", False)
        if invalid:
            if constant:
                self.write("Invalid (empty) as well as constant values are accepted.\n")
            else:
                self.write("Invalid (empty) calibrations are accepted.\n")
        elif constant:
            self.write("Constant values are accepted.\n")
        self.write("--\n")

    def option_variable(self, option, name, display, description, default):
        self.write("*--%s*=VARIABLE::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.generic_context(name, "variable")

    def option_input(self, option, name, display, description, default):
        self.write("*--%s*=INPUT::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.generic_context(name, "input")

    def option_operate(self, option, name, display, description, default):
        self.write("*--%s*=SELECTION::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.generic_context(name, "operate")

    def option_enum(self, option, name, display, description, default, values):
        self.write("*--%s*=TYPE::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.generic_context(name, "enum", False)
        if len(values) > 0:
            self.write("The possible values are:\n")
            self.write("\n")
            for v in values:
                self.write("--%s=*%s*;;\n" % (name, v["name"]))
                if "origin" in v:
                    self.write("Same as --%s=*%s*" % (name, v["origin"]["name"]))
                else:
                    self.write(v["description"])
                self.write("\n")
        self.write("\n")
        self.write("--\n")

    def option_interval(self, option, name, display, description, default, undefined, zero,
                        negative, default_interval):
        self.write("*--%s*=INTERVAL::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.generic_context(name, "interval", False)
        if default_interval is not None and default_interval.unit is not None:
            self.write("When no units are specified the interval is assumed to be in %s.\n" % default_interval.unit.printable(0))
        if undefined:
            self.write("Undefined intervals are accepted.\n")
        if not zero:
            if not negative:
                self.write("The interval must be greater than zero in length.\n")
            else:
                self.write("The interval cannot be exactly zero in length.\n")
        elif not negative:
            self.write("The interval must be greater than or equal to zero length.\n")
        self.write("\n")
        self.write("--\n")

    def option_interval_block(self, option, name, display, description, default, default_interval):
        self.write("*--%s*=INTERVALBLOCK::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.generic_context(name, "intervalblock", False)
        if default_interval is not None and default_interval.unit is not None:
            self.write("When no units are specified the interval is assumed to be in %s.\n" % default_interval.unit.printable(0))
        self.write("\n")
        self.write("--\n")

    def option_time_offset(self, option, name, display, description, default, zero,
                           negative, default_interval):
        self.write("*--%s*=OFFSET::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.generic_context(name, "timeoffset", False)
        if default_interval is not None and default_interval.unit is not None:
            self.write("When no units are specified the offset is assumed to be in %s.\n" % default_interval.unit.printable(0))
        if default_interval is not None and default_interval.aligned is not None:
            self.write("Unless explicitly disabled, the offset is assumed to be aligned.\n")
        if not zero:
            if not negative:
                self.write("The interval must be greater than zero in length.\n")
            else:
                self.write("The interval cannot be exactly zero in length.\n")
        elif not negative:
            self.write("The interval must be greater than or equal to zero length.\n")
        self.write("\n")
        self.write("--\n")

    def option_dynamic_string(self, option, name, display, description, default):
        self.write("*--%s*=STRINGS::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.generic_context(name, "dynamicstring")

    def option_dynamic_boolean(self, option, name, display, description, default):
        self.write("*--%s*[=BOOLEANS]::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.generic_context(name, "dynamicboolean")

    def option_dynamic_double(self, option, name, display, description, default, undefined, minimum,
                              maximum, minimum_exclusive, maximum_exclusive):
        self.write("*--%s*=NUMBERS::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.generic_context(name, "dynamicdouble", False)
        self._double_range(name, undefined, minimum, maximum, minimum_exclusive, maximum_exclusive)
        self.write("--\n")

    def option_dynamic_integer(self, option, name, display, description, default, undefined,
                               minimum, maximum):
        self.write("*--%s*=INTEGERS::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.generic_context(name, "dynamicinteger", False)
        self._integer_range(name, undefined, minimum, maximum)
        self.write("--\n")

    def option_dynamic_calibration(self, option, name, display, description, default):
        self.write("*--%s*=COEFFICIENTS...::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.generic_context(name, "dynamiccalibration")

    def option_smoother(self, option, name, display, description, default, stability, spike):
        self.write("*--%s*=SMOOTHER::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.generic_context(name, "smoother", False)
        if stability:
            if spike:
                self.write("The smoother is used for stability and spike detection.\n")
            else:
                self.write("The smoother is used for stability detection.\n")
        elif spike:
            self.write("The smoother is used for spike detection.\n")
        self.write("\n")
        self.write("--\n")

    def option_ssl(self, option, name, display, description, default):
        self.write("*--%s*=KEY,CERTIFICATE::\n" % name)
        self.body_generic(option, name, display, description, default)
        self.generic_context(name, "ssl")


_Options(input.findall('options/*'), output).run()
