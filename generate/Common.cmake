find_program(CPD3_DA NAMES da da.exe
             PATHS "${CPD3_BUILD}/cli")
find_program(CPD3_DISPLAY_HANDLER NAMES cpd3_display_handler cpd3_display_handler.exe
             PATHS "${CPD3_BUILD}/standlone/display_handler")

set(CONVERT_HELP_BASE ${CMAKE_CURRENT_SOURCE_DIR}/help)

set(APPLY_ASCIIDOC_PATCHES ${CMAKE_CURRENT_SOURCE_DIR}/apply_patches.py)

list(APPEND MAN_DEPENDS ${CPD3_SHARED_DIRECTORY}/man_tail.adoc)
list(APPEND MAN_DEPENDS ${CPD3_SHARED_DIRECTORY}/man_station.adoc)
list(APPEND MAN_DEPENDS ${CPD3_SHARED_DIRECTORY}/man_variables.adoc)
list(APPEND MAN_DEPENDS ${CPD3_SHARED_DIRECTORY}/man_times.adoc)
list(APPEND MAN_DEPENDS ${CPD3_SHARED_DIRECTORY}/man_archive.adoc)
list(APPEND MAN_DEPENDS ${CPD3_SHARED_DIRECTORY}/man_file.adoc)

list(APPEND CLI_DEPENDS ${CPD3_SHARED_DIRECTORY}/cli_station.adoc)
list(APPEND CLI_DEPENDS ${CPD3_SHARED_DIRECTORY}/cli_variables.adoc)
list(APPEND CLI_DEPENDS ${CPD3_SHARED_DIRECTORY}/cli_times.adoc)
list(APPEND CLI_DEPENDS ${CPD3_SHARED_DIRECTORY}/cli_archive.adoc)
list(APPEND CLI_DEPENDS ${CPD3_SHARED_DIRECTORY}/cli_file.adoc)
list(APPEND CLI_DEPENDS ${CPD3_LISTS_DIRECTORY}/cli_links.adoc)

function(make_help_xml)
    add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/help.xml
                       COMMAND ${ARGN} > ${CMAKE_CURRENT_BINARY_DIR}/help.xml)
endfunction(make_help_xml)

function(convert_xml_type type)
    cmake_parse_arguments(convert "" "INPUT" "PATCHES" ${ARGN})
    if("${convert_INPUT}" STREQUAL "")
        set(convert_INPUT "help.xml")
    endif("${convert_INPUT}" STREQUAL "")

    set(patches "")
    foreach(f IN LISTS convert_PATCHES)
        if(f MATCHES ".*\\_${type}_xml.py")
            list(APPEND patches "${CMAKE_CURRENT_SOURCE_DIR}/${f}")
        endif()
    endforeach()

    set(converter ${CONVERT_HELP_BASE}_${type}.py)
    set(output ${type}_intermediate.adoc)
    add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${output}
                       COMMAND ${converter}
                       ${CMAKE_CURRENT_BINARY_DIR}/${convert_INPUT}
                       ${CMAKE_CURRENT_BINARY_DIR}/${output}.tmp
                       ${patches}
                       COMMAND mv ${CMAKE_CURRENT_BINARY_DIR}/${output}.tmp
                       ${CMAKE_CURRENT_BINARY_DIR}/${output}
                       DEPENDS ${converter} help.xml ${patches})
endfunction(convert_xml_type)

function(patch_asciidoc_intermediate type)
    cmake_parse_arguments(patch "" "" "PATCHES" ${ARGN})

    set(patches "")
    foreach(f IN LISTS patch_PATCHES)
        if(f MATCHES ".*\\_${type}.patch")
            list(APPEND patches "${CMAKE_CURRENT_SOURCE_DIR}/${f}")
        endif()
    endforeach()

    set(input ${type}_intermediate.adoc)
    set(output ${type}.adoc)
    add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${output}
                       COMMAND ${APPLY_ASCIIDOC_PATCHES}
                       ${CMAKE_CURRENT_BINARY_DIR}/${input}
                       ${CMAKE_CURRENT_BINARY_DIR}/${output}.tmp
                       ${patches}
                       COMMAND mv ${CMAKE_CURRENT_BINARY_DIR}/${output}.tmp
                       ${CMAKE_CURRENT_BINARY_DIR}/${output}
                       DEPENDS ${APPLY_ASCIIDOC_PATCHES} ${input} ${patches})
endfunction(patch_asciidoc_intermediate)

macro(cpd3_component class)
    cmake_parse_arguments(component "" "" "PATCHES" ${ARGN})

    get_filename_component(component ${CMAKE_CURRENT_SOURCE_DIR} NAME)

    make_help_xml(${CPD3_DA} --component=${component} --help=__xml__)

    set(types link_cli index_cli options_man examples_man man options_cli examples_cli cli)
    foreach(type ${types})
        convert_xml_type(${type} PATCHES ${component_PATCHES})
        patch_asciidoc_intermediate(${type} PATCHES ${component_PATCHES})
        list(APPEND deps "${type}.adoc")
    endforeach(type)

    add_custom_target(generate_input_component_${component}
                      DEPENDS ${deps}
                      SOURCES ${component_PATCHES}
                      COMMENT "AsciiDoc generation for component ${component}")
    add_dependencies(generate_all generate_input_component_${component})

    string(REPLACE "_" "." command_name "${component}")
    man_page("da.${command_name}.1" man.adoc
             ALIAS da.${component}.1
             ATTRIBUTES cli-man
             cli-option-directory=${CLI_OPTION_DIRECTORY}
             DEPENDS options_man.adoc examples_man.adoc
             ${CLI_OPTION_MAN} ${MAN_DEPENDS})

    html_page("cli/da_${component}.html" cli.adoc
              ATTRIBUTES cli-split
              cli-option-directory=${CLI_OPTION_DIRECTORY}
              DEPENDS options_cli.adoc examples_cli.adoc
              ${CLI_OPTION_SPLIT} ${CLI_DEPENDS})

    list(APPEND CPD3_GENERATE "${component}")
    set(CPD3_GENERATE ${CPD3_GENERATE} PARENT_SCOPE)
    set(CPD3_GENERATE_TYPE_${component} "component" PARENT_SCOPE)
    set(CPD3_GENERATE_CLASS_${component} "${class}" PARENT_SCOPE)
endmacro(cpd3_component)


macro(cpd3_display class)
    cmake_parse_arguments(display "" "" "PATCHES" ${ARGN})

    get_filename_component(display ${CMAKE_CURRENT_SOURCE_DIR} NAME)

    make_help_xml(${CPD3_DISPLAY_HANDLER} --type=${display} --help=__xml__)

    set(types link_cli index_cli options_man examples_man man options_cli examples_cli cli)
    foreach(type ${types})
        convert_xml_type(${type} PATCHES ${display_PATCHES})
        patch_asciidoc_intermediate(${type} PATCHES ${display_PATCHES})
        list(APPEND deps "${type}.adoc")
    endforeach(type)

    add_custom_target(generate_input_display_${display}
                      DEPENDS ${deps}
                      SOURCES ${display_PATCHES}
                      COMMENT "AsciiDoc generation for display ${display}")
    add_dependencies(generate_all generate_input_display_${display})

    string(REPLACE "_" "." command_name "${display}")
    man_page("da.${command_name}.1" man.adoc
             ALIAS da.${display}.1
             ATTRIBUTES cli-man
             cli-option-directory=${CLI_OPTION_DIRECTORY}
             DEPENDS options_man.adoc examples_man.adoc
             ${CLI_OPTION_MAN} ${MAN_DEPENDS})

    html_page("cli/da_${display}.html" cli.adoc
              ATTRIBUTES cli-split
              cli-option-directory=${CLI_OPTION_DIRECTORY}
              DEPENDS options_cli.adoc examples_cli.adoc
              ${CLI_OPTION_SPLIT} ${CLI_DEPENDS})

    list(APPEND CPD3_GENERATE "${display}")
    set(CPD3_GENERATE ${CPD3_GENERATE} PARENT_SCOPE)
    set(CPD3_GENERATE_TYPE_${display} "display" PARENT_SCOPE)
    set(CPD3_GENERATE_CLASS_${display} "${class}" PARENT_SCOPE)
endmacro(cpd3_display)


macro(cpd3_program class name)
    cmake_parse_arguments(program "" "FORMAL_NAME" "PATCHES" ${ARGN})

    configure_file(man.adoc man.adoc COPYONLY)
    configure_file(cli.adoc cli.adoc COPYONLY)
    configure_file(index_cli.adoc index_cli.adoc COPYONLY)
    configure_file(link_cli.adoc link_cli.adoc COPYONLY)

    get_filename_component(program ${CMAKE_CURRENT_SOURCE_DIR} NAME)
    if("${program_FORMAL_NAME}" STREQUAL "")
        string(REPLACE "_" "." command_name "${program}")
        set(program_FORMAL_NAME "da.${command_name}")
    endif("${program_FORMAL_NAME}" STREQUAL "")

    find_program(execute_name NAMES ${name} ${name}.exe
                 PATHS "${CPD3_BUILD}/standlone/${program}")
    make_help_xml(${execute_name} --help=__xml__)

    set(types options_man examples_man options_cli examples_cli)
    foreach(type ${types})
        convert_xml_type(${type} PATCHES ${program_PATCHES})
        patch_asciidoc_intermediate(${type} PATCHES ${program_PATCHES})
        list(APPEND deps "${type}.adoc")
    endforeach(type)

    add_custom_target(generate_input_program_${program}
                      DEPENDS ${deps}
                      SOURCES ${program_PATCHES}
                      COMMENT "AsciiDoc generation for program ${program}")
    add_dependencies(generate_all generate_input_program_${program})

    man_page("${program_FORMAL_NAME}.1" man.adoc
             ALIAS ${name}.1
             ATTRIBUTES cli-man
             cli-option-directory=${CLI_OPTION_DIRECTORY}
             DEPENDS options_man.adoc examples_man.adoc
             ${CLI_OPTION_MAN} ${MAN_DEPENDS})

    html_page("cli/da_${program}.html" cli.adoc
              ATTRIBUTES cli-split
              cli-option-directory=${CLI_OPTION_DIRECTORY}
              DEPENDS options_cli.adoc examples_cli.adoc
              ${CLI_OPTION_SPLIT} ${CLI_DEPENDS})

    list(APPEND CPD3_GENERATE "${program}")
    set(CPD3_GENERATE ${CPD3_GENERATE} PARENT_SCOPE)
    set(CPD3_GENERATE_TYPE_${program} "program" PARENT_SCOPE)
    set(CPD3_GENERATE_CLASS_${program} "${class}" PARENT_SCOPE)
endmacro(cpd3_program)