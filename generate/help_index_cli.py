#!/usr/bin/python3

import sys
import xml.etree.ElementTree
import xmlloader
import identifier

input = xmlloader.load(sys.argv[1], sys.argv[3:])
output = open(sys.argv[2], "w", encoding="utf-8")

id = identifier.to_id(input)
htype = identifier.to_type(input)

summary = input.find("identifier/display")
if summary is not None:
    summary = summary.text.strip()
    if len(summary) == 0:
        summary = None



if htype == "filter" or htype == "read" or htype == "external" or htype == "input" or htype == "action" or type == "archive":
    component = input.find("identifier/component").text
    command = "da." + component.replace("_", ".").lower()
elif htype == "display":
    component = input.find("identifier/output").text
    if input.find("options/file/[@id='file']") is not None:
        command = "da.plot." + component.replace("_", ".").lower()
    else:
        command = "da.show." + component.replace("_", ".").lower()
else:
    name = input.find("command/program")
    if name is not None:
        command = name.text.strip()
    else:
        raise Exception("No command available")

if summary is not None:
    output.write("* <<{cli-generate-link-%s},%s>> - %s\n" % (id, command, summary))
else:
    output.write("* <<{cli-generate-link-%s},%s>>\n" % (id, command))