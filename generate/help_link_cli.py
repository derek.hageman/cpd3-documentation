#!/usr/bin/python3

import sys
import os
import os.path
import xml.etree.ElementTree
import xmlloader
import identifier

input = xmlloader.load(sys.argv[1], sys.argv[3:])
output = open(sys.argv[2], "w", encoding="utf-8")

id = identifier.to_id(input)
file = identifier.to_cli_file(input)

path = os.path.basename(os.getcwd())

output.write("ifdef::cli-single[]\n")
output.write(":cli-generate-link-%s: generate/%s/cli#cli-generate-%s\n" % (id, path, id))
output.write("endif::cli-single[]\n")

output.write("ifdef::cli-split[]\n")
output.write(":cli-generate-link-%s: %s#\n" % (id, file))
output.write("endif::cli-split[]\n")