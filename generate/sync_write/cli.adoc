[[cli-generate-sync_write]]
= da.sync.write


ifdef::cli-split[]
include::{cli-option-directory}/include_links.adoc[]
include::{cpd3-shared-directory}/include_links.adoc[]
include::{cpd3-shared-directory}/../lists/cli_links.adoc[]
include::{cpd3-shared-directory}/../lists/cli_links.adoc[]
endif::cli-split[]


This program examines the archive for changes and writes them to a specialized output stream.
That output stream is then used in conjunction with <<{cli-generate-link-sync_read},da.sync.read>> to perform manual synchronization.


== Usage

*da.sync.write* [switches...] [station] [file]


== Switches

ifdef::cpd3-final[]

include::options_cli.adoc[]

endif::cpd3-final[]


== Arguments

*station*::
+
--
This argument is used to specify the <<{shared-link-data-components},station>> that synchronization data are written out for.
The station is the three letter http://gawsis.meteoswiss.ch/[GAW station code] of the location, such as `_BND_`.
--
*file*::
+
--
This argument is used to specify the the file to write data to.
If it is absent or set to `_-_` (a single dash) then the stream is written to standard output.
--