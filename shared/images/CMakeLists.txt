
file(GLOB all_images RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} *.png)

foreach(image ${all_images})
    configure_file(${image} ${image} COPYONLY)

    if(INSTALL_HTML)
        install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${image}
                DESTINATION ${HTML_INSTALL_PATH}/shared/images)
    endif(INSTALL_HTML)

    list(APPEND CPD3_IMAGES ${CMAKE_CURRENT_BINARY_DIR}/${image})
endforeach(image)

set(CPD3_IMAGES ${CPD3_IMAGES} PARENT_SCOPE)