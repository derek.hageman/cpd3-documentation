= CPD3OPERATE(5)
:man source:  CPD3
:man version: {revnumber}
:man manual:  CPD3 Manual

== NAME

cpd3operate - Data parameter operation specifications

== DESCRIPTION

Data operation options select parameters in the data to perform an operation on.
In the simplest form this consists of just a variable name specification.
This results in operating on all values matching the variable name.

.Simple variable
----------
--switch=BsG_S11
----------

However, the full data selection cpd3variable(5) is also supported.

If the option contains a */* then it is treated as time dependent.
In this context the value is split into fields separated by *,* with the first field being the selection and all further ones specifying the effective bounds list as in cpd3time(5).
This means that the selection in the first field is applied for the time range specified in subsequent ones.

.Time dependency
----------
--switch=BsB_S11,,2016:10/BsB_S11,2016:10
--switch='BsB_S11;BsG_S11,,2016/Bs[RB]_S11,2016'
----------



ifdef::cpd3-final[]
include::{cpd3-shared-directory}/man_tail.adoc[]
endif::cpd3-final[]