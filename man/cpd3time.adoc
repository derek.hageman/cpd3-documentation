= CPD3TIME(5)
:man source:  CPD3
:man version: {revnumber}
:man manual:  CPD3 Manual

== NAME

cpd3time - Absolute time specifications

== DESCRIPTION

CPD3 accepts a number of formats that to can interpret times from.
This document only covers the formats accepted for absolute points in time.
For a discussion of the formats accepted for offsets and relative times, see cpd3interval(5).

== SINGLE TIME

The first and most common case for time interpretation is that of a single point in time.
This means a single specification of an absolute point in time rather than a range of time covered.

ifdef::cpd3-final[]
include::{cpd3-shared-directory}/time_single.adoc[]
endif::cpd3-final[]

== BOUNDS LIST

ifdef::cpd3-final[]
include::{cpd3-shared-directory}/time_bounds_list.adoc[]
endif::cpd3-final[]



ifdef::cpd3-final[]
include::{cpd3-shared-directory}/man_tail.adoc[]
endif::cpd3-final[]