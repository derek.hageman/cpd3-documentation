= CPD3INTERVAL(5)
:man source:  CPD3
:man version: {revnumber}
:man manual:  CPD3 Manual

== NAME

cpd3interval - CPD3 time interval and offset specifications

== DESCRIPTION

ifdef::cpd3-final[]
include::{cpd3-shared-directory}/time_interval.adoc[]
endif::cpd3-final[]



ifdef::cpd3-final[]
include::{cpd3-shared-directory}/man_tail.adoc[]
endif::cpd3-final[]