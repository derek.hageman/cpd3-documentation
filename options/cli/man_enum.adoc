This option accepts a single value from a list of possible ones.
The value name is not case sensitive.