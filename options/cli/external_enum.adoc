[[cli-option-enum]]
= Enumeration Option

ifdef::cli-split[]
include::{cpd3-shared-directory}/include_links.adoc[]
endif::cli-split[]

Enumeration options accept a single value from a set of possible ones.
Values are specified using a string of text and are not case sensitive.

For example a mode option might allow the values `_instrument_` and `_variable_` so exactly one of them would be valid.

.Set value
----------
--switch=value
----------

Some values may also be aliases for another one.
In this mode specifying the alias value has the same effect as specifying the main one.
For example if the value `_Excel_` is an alias for `_xl_` then either one can be used and it will have the same effect.