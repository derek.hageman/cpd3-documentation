[[cli-option-timeoffset]]
= Time Offset Option

ifdef::cli-split[]
include::{cpd3-shared-directory}/include_links.adoc[]
endif::cli-split[]

Time offset options accept a single <<{shared-link-time-interval},time interval>>.
The offset may be restricted to being non-zero and/or only positive.

.Simple interval
----------
--switch=1h
----------

Depending on the context the option may also permit zero intervals.
Similarly it may accept an undefined interval:
--
`_undef_`::
`_undefined_`::
`_none_`::
--

When one of these is used, the result of the offset is an undefined time.

.Undefined interval
----------
--switch=undef
--switch=undefined
----------