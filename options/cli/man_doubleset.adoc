This option accepts multiple unique base ten numbers with optional decimal points or scientific notation using 'e'.
Numbers are separated by _,_ or _;_ or _:_ and duplicates are ignored.