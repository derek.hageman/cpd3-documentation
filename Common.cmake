include(CMakeParseArguments)

set(ENV{DISPLAY} "")
set(ENV{CPD3_DISABLEGRAPHICS} "1")

find_program(ASCIIDOCTOR NAMES asciidoctor)
if(NOT ASCIIDOCTOR)
    message(FATAL_ERROR "AsciiDoctor not found")
endif(NOT ASCIIDOCTOR)


execute_process(COMMAND ${ASCIIDOCTOR} --doctype=manpage -
                INPUT_FILE /dev/null
                RESULT_VARIABLE ASCIIDOCTOR_MAN
                OUTPUT_QUIET ERROR_QUIET)
if(NOT ASCIIDOCTOR_MAN EQUAL "0" AND INSTALL_MAN)
    message(FATAL_ERROR "The installed AsciiDoctor version cannot create man pages")
endif(NOT ASCIIDOCTOR_MAN EQUAL "0" AND INSTALL_MAN)

find_program(GZIP NAMES gzip)
if(NOT GZIP AND INSTALL_MAN)
    message(FATAL_ERROR "gzip not found")
endif(NOT GZIP AND INSTALL_MAN)


find_program(ASCIIDOCTORPDF NAMES asciidoctor-pdf)
if(NOT ASCIIDOCTORPDF AND INSTALL_PDF)
    message(FATAL_ERROR "AsciiDoctor-PDF not found")
endif(NOT ASCIIDOCTORPDF AND INSTALL_PDF)


function(man_page name input)
    cmake_parse_arguments(man_page "" "ID" "ALIAS;DEPENDS;ATTRIBUTES" ${ARGN})
    if("${man_page_ID}" STREQUAL "")
        set(man_page_ID "${name}-build")
    endif("${man_page_ID}" STREQUAL "")

    set(additional_arguments "")
    foreach(attr IN LISTS man_page_ATTRIBUTES)
        list(APPEND additional_arguments -a)
        list(APPEND additional_arguments ${attr})
    endforeach(attr)

    add_custom_command(OUTPUT ${name}.gz
                       COMMAND ${ASCIIDOCTOR}
                       -a cpd3-final -a cpd3-shared-directory=${CPD3_SHARED_DIRECTORY}
                       ${additional_arguments}
                       --doctype=manpage --backend=manpage
                       "${CMAKE_CURRENT_BINARY_DIR}/${input}"
                       COMMAND ${GZIP} -f -q "${CMAKE_CURRENT_BINARY_DIR}/${name}"
                       DEPENDS "${CMAKE_CURRENT_BINARY_DIR}/${input}" ${MAN_TAIL} ${man_page_DEPENDS})


    if(INSTALL_MAN)
        add_custom_target(${man_page_ID} ALL DEPENDS ${name}.gz)

        string(REGEX REPLACE ".+\\.([1-9])$" "\\1" man_section ${name})
        set(man_path ${MAN_INSTALL_PATH}/man${man_section})
        install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${name}.gz DESTINATION ${man_path})

        if(NOT "${man_path}" MATCHES "^/")
            set(man_path "\$ENV{DESTDIR}\${CMAKE_INSTALL_PREFIX}/${man_path}")
        endif()
        foreach(linkname IN LISTS man_page_ALIAS)
            if(NOT linkname STREQUAL name)
                install(CODE "message(\"${man_path}/${linkname}.gz\" \) ")
                install(CODE "execute_process(COMMAND ln -f -s \"${name}.gz\" \"${man_path}/${linkname}.gz\" \) ")
            endif(NOT linkname STREQUAL name)
        endforeach(linkname)
    else(INSTALL_MAN)
        add_custom_target(${man_page_ID} DEPENDS ${name}.gz)
    endif(INSTALL_MAN)

    add_dependencies(man ${man_page_ID})

endfunction(man_page)

function(html_page install_target input)
    cmake_parse_arguments(html_page "" "DOCTYPE;ID" "DEPENDS;ATTRIBUTES" ${ARGN})
    if("${html_page_ID}" STREQUAL "")
        string(REPLACE "/" "_" html_page_ID "${install_target}")
        set(html_page_ID "${html_page_ID}-build")
    endif("${html_page_ID}" STREQUAL "")
    if("${html_page_DOCTYPE}" STREQUAL "")
        set(html_page_DOCTYPE "book")
    endif("${html_page_DOCTYPE}" STREQUAL "")

    set(additional_arguments "")
    foreach(attr IN LISTS html_page_ATTRIBUTES)
        list(APPEND additional_arguments -a)
        list(APPEND additional_arguments ${attr})
    endforeach(attr)

    get_filename_component(target_directory "${install_target}" DIRECTORY)
    get_filename_component(target_file "${install_target}" NAME)
    file(RELATIVE_PATH css_path "/${HTML_INSTALL_PATH}/${target_directory}" "/${HTML_INSTALL_PATH}/stylesheets")


    add_custom_command(OUTPUT ${input}.html
                       COMMAND ${ASCIIDOCTOR}
                       -a cpd3-final -a cpd3-shared-directory=${CPD3_SHARED_DIRECTORY}
                       -a linkcss -a stylesheet=cpd3.css -a stylesdir="${css_path}" -a copycss!
                       ${additional_arguments}
                       --doctype=${html_page_DOCTYPE} --backend=html
                       --out-file="${CMAKE_CURRENT_BINARY_DIR}/${input}.html"
                       "${CMAKE_CURRENT_BINARY_DIR}/${input}"
                       DEPENDS "${CMAKE_CURRENT_BINARY_DIR}/${input}" ${html_page_DEPENDS}
                       WORKING_DIRECTORY ${CMAKE_BINARY_DIR})

    if(INSTALL_HTML)
        add_custom_target(${html_page_ID} ALL DEPENDS ${input}.html)

        install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${input}.html
                DESTINATION ${HTML_INSTALL_PATH}/${target_directory}
                RENAME ${target_file})
    else(INSTALL_HTML)
        add_custom_target(${html_page_ID} DEPENDS ${target_file})
    endif(INSTALL_HTML)

    add_dependencies(html ${html_page_ID})
endfunction(html_page)

function(pdf install_target input)
    cmake_parse_arguments(pdf "" "DOCTYPE;ID" "DEPENDS;ATTRIBUTES" ${ARGN})
    if("${pdf_ID}" STREQUAL "")
        string(REPLACE "/" "_" pdf_ID "${install_target}")
        set(pdf_ID "${pdf_ID}-build")
    endif("${pdf_ID}" STREQUAL "")
    if("${pdf_DOCTYPE}" STREQUAL "")
        set(pdf_DOCTYPE "book")
    endif("${pdf_DOCTYPE}" STREQUAL "")

    set(additional_arguments "")
    foreach(attr IN LISTS pdf_ATTRIBUTES)
        list(APPEND additional_arguments -a)
        list(APPEND additional_arguments ${attr})
    endforeach(attr)

    get_filename_component(target_directory "${install_target}" DIRECTORY)
    get_filename_component(target_file "${install_target}" NAME)

    add_custom_command(OUTPUT ${input}.pdf
                       COMMAND ${ASCIIDOCTORPDF}
                       -a cpd3-final -a cpd3-shared-directory=${CPD3_SHARED_DIRECTORY}
                       -a imagesdir=shared/images
                       ${additional_arguments}
                       --doctype=${pdf_DOCTYPE}
                       --out-file="${CMAKE_CURRENT_BINARY_DIR}/${input}.pdf"
                       "${CMAKE_CURRENT_BINARY_DIR}/${input}"
                       DEPENDS "${CMAKE_CURRENT_BINARY_DIR}/${input}" ${pdf_DEPENDS})

    if(INSTALL_PDF)
        add_custom_target(${pdf_ID} ALL DEPENDS ${input}.pdf)

        install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${input}.pdf
                DESTINATION ${PDF_INSTALL_PATH}/${target_directory}
                RENAME ${target_file})
    else(INSTALL_PDF)
        add_custom_target(${pdf_ID} DEPENDS ${input}.pdf)
    endif(INSTALL_PDF)

    add_dependencies(pdf ${pdf_ID})
endfunction(pdf)
