[[shared-link-data-components]]
= CPD3 Data Parameter Components

ifdef::cpd3-final[]
include::shared/include_links.adoc[]
endif::cpd3-final[]


Data specifications in CPD3 are broken down into several components.
In most contexts these components have default values that will be filled in if they are not explicitly specified.

include::shared/data_components.adoc[]